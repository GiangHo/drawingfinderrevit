﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration.Install;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace InstallerHelper.RV2019
{
    /// <summary>
    /// This installer class overrides the base installer class to provide
    /// additional functionality for the Custom Actions.
    /// </summary>
    [RunInstaller(true)]
    public class InstallerHelper : Installer
    {
        static void Main()
        { }
        /// <summary>
        /// Constructor method
        /// </summary>
        public InstallerHelper()
            : base()
        {
        }

        public override void Install(IDictionary stateSaver)
        {
            var p = Process.GetProcessesByName("Revit").FirstOrDefault();

            if (p != null)
                throw new InstallException("Please close all Revit windows and try agian!");

            base.Install(stateSaver);
        }

        protected override void OnAfterInstall(IDictionary savedState)
        {
            base.OnAfterInstall(savedState);
            try
            {
                string addInFilePath = InstallFolder() + @"\Simpson Strong-Tie\Drawing Finder for Revit\Revit 2019\DrawingFinderForRevit2019.addin";
                var fileInfo = new FileInfo(addInFilePath);
                var revit2019Folder = @"C:\ProgramData\Autodesk\Revit\Addins\2019";

                if (Directory.Exists(revit2019Folder))
                    fileInfo.CopyTo(revit2019Folder + @"\DrawingFinderForRevit2019.addin", true);


            }
            catch
            {
            }
        }
        private static string InstallFolder()
        {
            if (8 == IntPtr.Size || (!String.IsNullOrEmpty(Environment.GetEnvironmentVariable("PROCESSOR_ARCHITEW6432"))))
            {
                return Environment.GetEnvironmentVariable("ProgramFiles(x86)");
            }

            return Environment.GetEnvironmentVariable("ProgramFiles");
        }

        protected override void OnBeforeUninstall(IDictionary savedState)
        {
            base.OnBeforeUninstall(savedState);
            try
            {
                var revit2019Folder = @"C:\ProgramData\Autodesk\Revit\Addins\2019";

                if (Directory.Exists(revit2019Folder))
                    File.Delete(revit2019Folder + @"\DrawingFinderForRevit2019.addin");

            }
            catch
            {
            }
        }

    }
}
