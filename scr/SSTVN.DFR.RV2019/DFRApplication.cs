﻿using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using CefSharp;
using Serilog;
using SSTVN.DFR.RV2019.ViewModels;
using SSTVN.DFR.RV2019.Views;
using System;
using System.IO;
using System.Windows.Media.Imaging;

namespace SSTVN.DFR.RV2019
{

    [Transaction(TransactionMode.Manual)]
    [Regeneration(RegenerationOption.Manual)]
    public class DFRApplication : IExternalApplication
    {
        public DFRApplication()
        {
        }

        public void RegisterDockableWindow(UIControlledApplication application, Guid mainPageGuid)
        {

            Constants.DrawingFinderDockablePaneId = new DockablePaneId(mainPageGuid);
            application.RegisterDockablePane(Constants.DrawingFinderDockablePaneId, Constants.ApplicationName, DFRApplication.thisApp.GetMainWindow() as IDockablePaneProvider);
        }
        /// <summary>
        /// Add UI for registering, showing, and hiding dockable panes.
        /// </summary>
        public Result OnStartup(UIControlledApplication application)
        {
            thisApp = this;
            #region init logger
            //Init logger configuration
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.File("Logs/log-.txt"
                    , rollingInterval: RollingInterval.Day
                    , outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Exception}")
                .CreateLogger();
            Log.Information("Application start");
            #endregion
            #region init cefsharp
            try
            {
                float.TryParse(application.ControlledApplication.VersionNumber, out Revit_Version);
                //Revit version > 2019 had been embedment Cefsharp
                if (Revit_Version < 2019)
                {
                    var libraryLoader = new CefLibraryHandle(Path.Combine(Constants.CefSharpFolder, "libcef.dll"));
                    CefSettings settings = new CefSettings();
                    settings.BrowserSubprocessPath = Path.Combine(Constants.CefSharpFolder, "CefSharp.BrowserSubprocess.exe");
                    settings.LocalesDirPath = Path.Combine(Constants.CefSharpFolder, "locales");
                    settings.ResourcesDirPath = Constants.CefSharpFolder;
                    // Make sure you set performDependencyCheck false
                    Cef.Initialize(settings, performDependencyCheck: false, browserProcessHandler: null);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Can not initalizer Cefsharp");
            }
            #endregion
            //Application.Current = this.DrawingFinderView;
            #region Ribbon init
            application.CreateRibbonTab(Constants.DiagnosticsTabName);
            RibbonPanel panel = application.CreateRibbonPanel(Constants.DiagnosticsTabName.ToString(), Constants.DiagnosticsPanelName);
            // Drawing Finder Button
            PushButtonData pushButtonDrawingFinderData = new PushButtonData(Constants.BtnDrawingFinderName, Constants.BtnDrawingFinderName, FileUtility.GetAssemblyFullName(), typeof(ExternalCommandShowDrawingFinder).FullName);
            pushButtonDrawingFinderData.LargeImage = new BitmapImage(new Uri(FileUtility.GetApplicationResourcesPath() + "/Icons/drawingFinder.png"));
            pushButtonDrawingFinderData.ToolTip = Constants.BtnDrawingFinderToolTip;
            PushButton pushButtonDrawingFinder = panel.AddItem(pushButtonDrawingFinderData) as PushButton;

            // DWG Drawing Finder Button
            PushButtonData pushButtonDWGFinderButtonData = new PushButtonData(Constants.BtnDWGDrawingFinderName, Constants.BtnDWGDrawingFinderName, FileUtility.GetAssemblyFullName(), typeof(ExternalCommandShowDWGDrawingFinder).FullName);
            pushButtonDWGFinderButtonData.LargeImage = new BitmapImage(new Uri(FileUtility.GetApplicationResourcesPath() + "/Icons/dwgImport.png"));
            pushButtonDWGFinderButtonData.ToolTip = Constants.BtnDWGDrawingFinderToolTip;
            PushButton pushButtonDWGFinder = panel.AddItem(pushButtonDWGFinderButtonData) as PushButton;

            // Downloaded Drawings button
            PushButtonData pushButtonDownloadedDrawingsData = new PushButtonData(Constants.BtnDownloadedDrawingsName, Constants.BtnDownloadedDrawingsName, FileUtility.GetAssemblyFullName(), typeof(ExternalCommandShowDownloadedDrawing).FullName);
            pushButtonDownloadedDrawingsData.LargeImage = new BitmapImage(new Uri(FileUtility.GetApplicationResourcesPath() + "/Icons/downloadedDrawings.png"));
            pushButtonDownloadedDrawingsData.ToolTip = Constants.BtnDownloadedDrawingsToolTip;
            PushButton pushButtonDownloadedDrawings = panel.AddItem(pushButtonDownloadedDrawingsData) as PushButton;

            panel.AddSeparator();

            // Other application button
            PushButtonData pushButtonOtherApplicationData = new PushButtonData(Constants.BtnOtherApplicationName, Constants.BtnOtherApplicationName, FileUtility.GetAssemblyFullName(), typeof(ExternalCommandOtherApplication).FullName);
            pushButtonOtherApplicationData.LargeImage = new BitmapImage(new Uri(FileUtility.GetApplicationResourcesPath() + "/Icons/otherApplications.png"));
            pushButtonOtherApplicationData.ToolTip = Constants.BtnOtherApplicationToolTip;
            PushButton pushButtonOtherApplication = panel.AddItem(pushButtonOtherApplicationData) as PushButton;

            // Settings Button
            PushButtonData pushButtonSettingsData = new PushButtonData(Constants.BtnSettingsName, Constants.BtnSettingsName, FileUtility.GetAssemblyFullName(), typeof(ExternalCommandSettings).FullName);
            pushButtonSettingsData.LargeImage = new BitmapImage(new Uri(FileUtility.GetApplicationResourcesPath() + "/Icons/settings.png"));
            //pushButtonSettingsData.ToolTip = Constants.BtnSettingToolTip;

            // Tutorial Button
            PushButtonData pushButtonTutorialData = new PushButtonData(Constants.BtnTutorialName, Constants.BtnTutorialName, FileUtility.GetAssemblyFullName(), typeof(ExternalCommandTutorial).FullName);
            pushButtonTutorialData.LargeImage = new BitmapImage(new Uri(FileUtility.GetApplicationResourcesPath() + "/Icons/tutorial.png"));
            pushButtonTutorialData.ToolTip = Constants.BtnTutorialToolTip;

            // About Button
            PushButtonData pushButtonAboutData = new PushButtonData(Constants.BtnABoutName, Constants.BtnABoutName, FileUtility.GetAssemblyFullName(), typeof(ExternalCommandAbout).FullName);
            pushButtonAboutData.LargeImage = new BitmapImage(new Uri(FileUtility.GetApplicationResourcesPath() + "/Icons/about.png"));
            //pushButtonAboutData.ToolTip = Constants.BtnABoutToolTip;

            // Request Button
            PushButtonData pushButtonRequestData = new PushButtonData(Constants.BtnRequestName, Constants.BtnRequestName, FileUtility.GetAssemblyFullName(), typeof(ExternalCommandRequest).FullName);
            pushButtonRequestData.LargeImage = new BitmapImage(new Uri(FileUtility.GetApplicationResourcesPath() + "/Icons/requestDrawings.png"));
            //pushButtonRequestData.ToolTip = Constants.BtnRequestToolTip;

            PulldownButtonData pushButtonHelpData = new PulldownButtonData(Constants.BtnHelpName, Constants.BtnHelpName);
            pushButtonHelpData.LargeImage = new BitmapImage(new Uri(FileUtility.GetApplicationResourcesPath() + "/Icons/help.png"));
            //pushButtonHelpData.ToolTip = Constants.BtnHelpToolTip;

            PulldownButton sbButtonHelp = panel.AddItem(pushButtonHelpData) as PulldownButton;
            sbButtonHelp.AddPushButton(pushButtonSettingsData);
            sbButtonHelp.AddPushButton(pushButtonTutorialData);
            sbButtonHelp.AddPushButton(pushButtonAboutData);
            sbButtonHelp.AddPushButton(pushButtonRequestData);

            #endregion

            CreateWindow();

            application.RegisterDockablePane(Constants.DrawingFinderDockablePaneId, Constants.ApplicationName, DFRApplication.thisApp.GetMainWindow() as IDockablePaneProvider);

            return Result.Succeeded;
        }


        /// <summary>
        /// Show seeting dialog
        /// </summary>
        internal void ShowSettings()
        {
            var settingView = new SettingView();
            settingView.ShowDialog();
        }

        /// <summary>
        /// Show DWG drawing finder web panel
        /// </summary>
        /// <param name="application"></param>
        public void ShowDWGDrawingFinderPanel(UIApplication application)
        {
            try
            {
                DockablePane pane = application.GetDockablePane(Constants.DrawingFinderDockablePaneId);
                if (pane != null)
                {
                    pane.Show();
                    DrawingFinderView.Browsers.Visibility = System.Windows.Visibility.Visible;
                    DrawingFinderView.RevitBrowser.Visibility = System.Windows.Visibility.Collapsed;
                    DrawingFinderView.DWGBrowser.Visibility = System.Windows.Visibility.Visible;
                    DrawingFinderView.DownloadedDrawings.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Show About dialog
        /// </summary>
        public void ShowAbout()
        {
            var aboutView = new AboutView();
            aboutView.ShowDialog();
        }

        /// <summary>
        /// Create the new WPF Pages that Revit will dock.
        /// </summary>
        public void CreateWindow()
        {
            // A new handler to handle request posting by the dialog
            Handler = new RequestHandler();
            // External Event for the dialog to use (to post requests)
            ExEvent = ExternalEvent.Create(Handler);
            DrawingFinderView = new DrawingFinderPage(ExEvent, Handler);
        }

        /// <summary>
        /// Show Drawing Finder DockablePanel
        /// </summary>
        public void ShowDrawingFinderPanel(Autodesk.Revit.UI.UIApplication application)
        {
            try
            {
                DockablePane pane = application.GetDockablePane(Constants.DrawingFinderDockablePaneId);
                if (pane != null)
                {
                    pane.Show();
                    DrawingFinderView.Browsers.Visibility = System.Windows.Visibility.Visible;
                    DrawingFinderView.RevitBrowser.Visibility = System.Windows.Visibility.Visible;
                    DrawingFinderView.DWGBrowser.Visibility = System.Windows.Visibility.Collapsed;
                    DrawingFinderView.DownloadedDrawings.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Show Downloaded Drawing DockablePanel
        /// </summary>
        public void ShowDownloadedDrawingsPanel(Autodesk.Revit.UI.UIApplication application)
        {
            try
            {
                DockablePane pane = application.GetDockablePane(Constants.DrawingFinderDockablePaneId);

                if (pane != null)
                {
                    pane.Show();
                    DrawingFinderView.Browsers.Visibility = System.Windows.Visibility.Collapsed;
                    DrawingFinderView.DownloadedDrawings.Visibility = System.Windows.Visibility.Visible;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool IsMainWindowAvailable()
        {

            if (DrawingFinderView == null)
                return false;

            bool isAvailable = true;
            try { bool isVisible = DrawingFinderView.IsVisible; }
            catch (Exception)
            {
                isAvailable = false;
            }
            return isAvailable;

        }
        public DrawingFinderPage GetMainWindow()
        {
            if (!IsMainWindowAvailable())
                throw new InvalidOperationException("Main window not constructed.");
            return DrawingFinderView;
        }
        public Result OnShutdown(UIControlledApplication application)
        {
            var vm = DrawingFinderView.DataContext as DrawingFinderVM;
            vm.ExEvent.Dispose();
            vm.ExEvent = null;
            vm.Handler = null;
            if (vm.DWGDownloadedFolder != null)
            {
                vm.DWGDownloadedFolder.Dispose();
            }
            if (vm.Downloaded3DRFAFolder != null)
            {
                vm.Downloaded3DRFAFolder.Dispose();
            }
            if (vm.Downloaded2DRFAFolder != null)
            {
                vm.Downloaded2DRFAFolder.Dispose();
            }
            return Result.Succeeded;
        }

        public Autodesk.Revit.UI.DockablePaneId MainPageDockablePaneId
        {

            get { return Constants.DrawingFinderDockablePaneId; }
        }
        #region Data
        public DrawingFinderPage DrawingFinderView;
        public RequestHandler Handler;
        public ExternalEvent ExEvent;
        public float Revit_Version;
        //public FileService fileService = new FileService();
        internal static DFRApplication thisApp = null;
        #endregion
    }

}
