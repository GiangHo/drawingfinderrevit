﻿//
// (C) Copyright 2003-2017 by Autodesk, Inc.
//
// Permission to use, copy, modify, and distribute this software in
// object code form for any purpose and without fee is hereby granted,
// provided that the above copyright notice appears in all copies and
// that both that copyright notice and the limited warranty and
// restricted rights notice below appear in all supporting
// documentation.
//
// AUTODESK PROVIDES THIS PROGRAM "AS IS" AND WITH ALL FAULTS.
// AUTODESK SPECIFICALLY DISCLAIMS ANY IMPLIED WARRANTY OF
// MERCHANTABILITY OR FITNESS FOR A PARTICULAR USE. AUTODESK, INC.
// DOES NOT WARRANT THAT THE OPERATION OF THE PROGRAM WILL BE
// UNINTERRUPTED OR ERROR FREE.
//
// Use, duplication, or disclosure by the U.S. Government is subject to
// restrictions set forth in FAR 52.227-19 (Commercial Computer
// Software - Restricted Rights) and DFAR 252.227-7013(c)(1)(ii)
// (Rights in Technical Data and Computer Software), as applicable.
//

using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System;

namespace SSTVN.DFR.RV2019
{

    /// <summary>
    /// Show Drawing Finder Page
    /// </summary>
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    public class ExternalCommandShowDrawingFinder : IExternalCommand
    {
        public virtual Result Execute(ExternalCommandData commandData
            , ref string message, ElementSet elements)
        {
            try
            {
                DFRApplication.thisApp.ShowDrawingFinderPanel(commandData.Application);
            }
            catch (Exception)
            {
                TaskDialog.Show("Drawing Finder Dockable Panel", "Dialog not registered.");
            }
            return Result.Succeeded;
        }
    }

    /// <summary>
    /// Show Drawing Finder Page
    /// </summary>
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    public class ExternalCommandShowDWGDrawingFinder : IExternalCommand
    {
        public virtual Result Execute(ExternalCommandData commandData
            , ref string message, ElementSet elements)
        {
            try
            {
                DFRApplication.thisApp.ShowDWGDrawingFinderPanel(commandData.Application);
            }
            catch (Exception)
            {
                TaskDialog.Show("Drawing Finder Dockable Panel", "Dialog not registered.");
            }
            return Result.Succeeded;
        }
    }

    /// <summary>
    /// Show Downloaded Drawing page
    /// </summary>
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    public class ExternalCommandShowDownloadedDrawing : IExternalCommand
    {
        public virtual Result Execute(ExternalCommandData commandData
            , ref string message, ElementSet elements)
        {
            try
            {
                DFRApplication.thisApp.ShowDownloadedDrawingsPanel(commandData.Application);
            }
            catch (Exception)
            {
                TaskDialog.Show("Downloaded drawing Dockable Panel", "Dialog not registered.");
            }
            return Result.Succeeded;
        }
    }

    /// <summary>
    /// Open Simpson other application web page
    /// </summary>
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    public class ExternalCommandOtherApplication : IExternalCommand
    {
        public virtual Result Execute(ExternalCommandData commandData
            , ref string message, ElementSet elements)
        {
            System.Diagnostics.Process.Start(Constants.OtherAppsWebPage);
            return Result.Succeeded;
        }
    }

    /// <summary>
    /// Show Settings Dialog
    /// </summary>
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    public class ExternalCommandSettings : IExternalCommand
    {
        public virtual Result Execute(ExternalCommandData commandData
            , ref string message, ElementSet elements)
        {
            DFRApplication.thisApp.ShowSettings();
            return Result.Succeeded;
        }
    }

    /// <summary>
    /// Open Revit Drawing Finder tutorial Web Page
    /// </summary>
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    public class ExternalCommandTutorial : IExternalCommand
    {
        public virtual Result Execute(ExternalCommandData commandData
            , ref string message, ElementSet elements)
        {
            System.Diagnostics.Process.Start(Constants.TutorialWebPage);
            return Result.Succeeded;
        }
    }



    /// <summary>
    /// Show Settings Dialog
    /// </summary>
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    public class ExternalCommandAbout : IExternalCommand
    {
        public virtual Result Execute(ExternalCommandData commandData
            , ref string message, ElementSet elements)
        {
            DFRApplication.thisApp.ShowAbout();
            return Result.Succeeded;
        }
    }

    /// <summary>
    /// Show Settings Dialog
    /// </summary>
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    public class ExternalCommandRequest : IExternalCommand
    {
        public virtual Result Execute(ExternalCommandData commandData
            , ref string message, ElementSet elements)
        {
            System.Diagnostics.Process.Start(Constants.RequestDrawingPage);
            return Result.Succeeded;
        }
    }
}
