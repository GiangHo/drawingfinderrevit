﻿using Autodesk.Revit.UI;
using CefSharp;
using Serilog;
using SSTVN.DFR.RV2019.Model;
using SSTVN.DFR.RV2019.Views;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace SSTVN.DFR.RV2019.ViewModels
{
    /// <summary>
    /// APP VM
    /// We have implement all in one view Due to we have an issue with FrameContent in WPF and external handler.
    /// framecontent has been disable when external command raise.
    /// </summary>
    public class DrawingFinderVM : BaseViewModel
    {
        #region private
        /// <summary>
        /// Main Page
        /// </summary>
        private DrawingFinderPage _mainPage;

        /// <summary>
        /// This object handler an request from modeless to revit
        /// </summary>
        public RequestHandler Handler;
        public ExternalEvent ExEvent;
        private QueryContinueDragEventHandler queryhandler;
        private System.Windows.Point _startPoint;
        private WebClient webClient1;
        private ProcessView _processView;
        private Visibility _isShow3DRFADownloadedFolder;
        private Visibility _isShow2DRFADownloadedFolder;
        private Visibility _isShowDWGDownloadedFolder;
        private DownloadHandler Downer;
        private LifeSpanHandler LifeSpaner;
        private string DownloadedFileFullName;

        #endregion


        #region Constructor
        public DrawingFinderVM(DrawingFinderPage view, ExternalEvent exEvent, RequestHandler handler)
        {

            _mainPage = view;
            Handler = handler;
            ExEvent = exEvent;
            //_mainPage.browser.Address = Settings.PluginLink;
            _mainPage.Loaded += _mainPage_Loaded;
            Downer = new DownloadHandler();
            LifeSpaner = new LifeSpanHandler();
            //creating handler for Drag and Drop 
            queryhandler = new QueryContinueDragEventHandler(DragSourceQueryContinueDrag);
            _mainPage.Browsers.Visibility = System.Windows.Visibility.Visible;
            _mainPage.RevitBrowser.Visibility = System.Windows.Visibility.Visible;
            _mainPage.RevitBrowser.DownloadHandler = Downer;
            _mainPage.RevitBrowser.LifeSpanHandler = LifeSpaner;
            _mainPage.RevitBrowser.LoadingStateChanged += Browser_LoadingStateChanged;

            _mainPage.DWGBrowser.Visibility = System.Windows.Visibility.Collapsed;
            _mainPage.DWGBrowser.DownloadHandler = Downer;
            _mainPage.DWGBrowser.LifeSpanHandler = LifeSpaner;
            _mainPage.DWGBrowser.LoadingStateChanged += Browser_LoadingStateChanged;

            _mainPage.DownloadedDrawings.Visibility = System.Windows.Visibility.Collapsed;
            _mainPage.PreviewMouseRightButtonDown += Main_PreviewMouseRightButtonDown;
            _mainPage.MouseLeftButtonDown += Main_MouseLeftButtonDown;
            _mainPage.MouseMove += Main_MouseMove;
            _mainPage.DownloadedDrawings.IsVisibleChanged += DownloadedDrawings_IsVisibleChanged;
            _mainPage.ButtonWrappanel.SizeChanged += ButtonWrappanel_SizeChanged;
            Downer.OnBeforeDownloadFired += OnBeforeDownloadFired;
            Downer.OnDownloadUpdatedFired += OnDownloadUpdatedFired;

            ICRefreshBrowser = new RelayCommand(() => RefreshBrowser());
            ICShow3DRFADownloadedDrawings = new RelayCommand(() => Show3DRFADownloadedDrawings());
            ICShow2DRFADownloadedDrawings = new RelayCommand(() => Show2DRFADownloadedDrawings());
            ICShowDWGDownloadedDrawings = new RelayCommand(() => ShowDWGDownloadedDrawings());
            ICRefreshDrawingList = new RelayCommand(() => RefreshDownloadedFolder());
            ICSort = new RelayCommand(() => SortClick());
            ICView = new RelayCommand(() => ViewClick());
            ICSortByName = new RelayCommand(() => SortByName());
            ICSortByDate = new RelayCommand(() => SortByDate());
            ICThumbNailView = new RelayCommand(() => ThumbNailView());
            ICIconView = new RelayCommand(() => IconView());

            ICInsertDownloadedDrawing = new RelayCommand(() => InsertDownloadedDrawing());
            ICommandGotoContainingFolder = new RelayCommand(() => GotoContainingFolder());
            ICommandDeleteDrawing = new RelayCommand(() => DeleteDrawing());
        }

        private void DownloadedDrawings_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if ((bool)e.NewValue)
                {

                    Show3DRFADownloadedDrawings();
                }
            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex.Message);
                throw ex;
            }
        }

        #endregion

        #region properties
        /// <summary>
        /// Binding to UI is drawing finder page display default web page or test page
        /// </summary>
        public string BrowserMode { get; set; } = Settings.IsTestingMode ? "Testing Mode" : "";

        /// <summary>
        /// Binding to UI refresh button image
        /// </summary>
        public string RefreshImageSource { get => FileUtility.GetApplicationResourcesPath() + "/Icons/refresh.png"; }

        /// <summary>
        /// Downloaded file thumnail image visibility
        /// </summary>
        public System.Windows.Visibility ThumbnailVisibility { get; set; } = System.Windows.Visibility.Visible;

        /// <summary>
        /// Downloaded file Icon image visibility
        /// </summary>
        public System.Windows.Visibility IconVisibility { get; set; } = System.Windows.Visibility.Collapsed;

        /// <summary>
        /// Insert downloaded drawing context menu visibility
        /// </summary>
        public System.Windows.Visibility InsertDownloadedDrawingVisibility { get; set; }

        /// <summary>
        /// Go to containing folder visibility
        /// </summary>
        public System.Windows.Visibility GotoContainingFolderVisibility { get; set; }

        /// <summary>
        /// Delete drawing context menu visibility
        /// </summary>
        public System.Windows.Visibility DeleteDrawingVisibility { get; set; }

        /// <summary>
        /// Set color brush for 3D FRA downloaded drawing
        /// </summary>
        public SolidColorBrush Button3DRFABackGroundBrush
        {
            get
            {
                if (Downloaded3DRFAFolderVisibility == Visibility.Visible)
                    return Settings.PanelBackGroundColorBrush;
                else
                    return Settings.TranparentColorBrush;
            }
        }

        /// <summary>
        /// Set color brush for 2D FRA downloaded drawing
        /// </summary>
        public SolidColorBrush Button2DRFABackGroundBrush
        {
            get
            {
                if (Downloaded2DRFAFolderVisibility == Visibility.Visible)
                    return Settings.PanelBackGroundColorBrush;
                else
                    return Settings.TranparentColorBrush;
            }
        }

        /// <summary>
        /// Set color brush for DWG downloaded drawing
        /// </summary>
        public SolidColorBrush ButtonDWGBackGroundBrush
        {
            get
            {
                if (Downloaded2DDWGFolderVisibility == Visibility.Visible)
                    return Settings.PanelBackGroundColorBrush;
                else
                    return Settings.TranparentColorBrush;
            }
        }

        /// <summary>
        /// RFA downloaded folder
        /// </summary>
        public DownloadedFolder Downloaded3DRFAFolder { get; set; }

        public DownloadedFolder Downloaded2DRFAFolder { get; set; }

        /// <summary>
        /// DWG downloaded folder
        /// </summary>
        public DownloadedFolder DWGDownloadedFolder { get; set; }

        private ObservableCollection<DownloadedFile> _downloaded3DRFAFilesItemSource;
        /// <summary>
        /// Obserable collection binding to listdrawing view
        /// </summary>
        public ObservableCollection<DownloadedFile> Downloaded3DRFAFilesItemSource
        {
            get => _downloaded3DRFAFilesItemSource;
            set
            {
                _downloaded3DRFAFilesItemSource = value;
                OnpropertyChanged("Downloaded3DRFAFilesItemSource");
            }
        }

        private ObservableCollection<DownloadedFile> _downloaded2DRFAFilesItemSource;

        public ObservableCollection<DownloadedFile> Downloaded2DRFAFilesItemSource
        {
            get => _downloaded2DRFAFilesItemSource;
            set
            {
                _downloaded2DRFAFilesItemSource = value;
                OnpropertyChanged("Downloaded2DRFAFilesItemSource");
            }
        }

        private ObservableCollection<DownloadedFile> _downloaded2DDWGFilesItemSource;
        public ObservableCollection<DownloadedFile> Downloaded2DDWGFilesItemSource
        {
            get => _downloaded2DDWGFilesItemSource;
            set
            {
                _downloaded2DDWGFilesItemSource = value;
                OnpropertyChanged("Downloaded2DDWGFilesItemSource");
            }
        }

        /// <summary>
        /// True if 3D RFA downloaded drawing is show on UI
        /// </summary>
        public Visibility Downloaded3DRFAFolderVisibility
        {
            get => _isShow3DRFADownloadedFolder;
            set
            {
                _isShow3DRFADownloadedFolder = value;
                OnpropertyChanged("IsShow3DRFADownloadedFolder");
                OnpropertyChanged("Button3DRFABackGroundBrush");
            }
        }

        /// <summary>
        /// True if 2D RFA downloaded drawing is show on UI
        /// </summary>
        public Visibility Downloaded2DRFAFolderVisibility
        {
            get => _isShow2DRFADownloadedFolder;
            set
            {
                _isShow2DRFADownloadedFolder = value;
                OnpropertyChanged("IsShow2DRFADownloadedFolder");
                OnpropertyChanged("Button2DRFABackGroundBrush");
            }
        }

        /// <summary>
        /// True if 2D DWG downloaded drawing is show on UI
        /// </summary>
        public Visibility Downloaded2DDWGFolderVisibility
        {
            get => _isShowDWGDownloadedFolder;
            set
            {
                _isShowDWGDownloadedFolder = value;
                OnpropertyChanged("IsShow2DDWGDownloadedFolder");
                OnpropertyChanged("ButtonDWGBackGroundBrush");
            }
        }

        /// <summary>
        /// Web browser loading status
        /// </summary>
        public string LoadingStatus { get; set; }

        /// <summary>
        /// Revit Browser web address
        /// </summary>
        public string RevitBrowserAddress { get; set; } = Settings.RevitLink;

        /// <summary>
        /// DWG bowser web address
        /// </summary>
        public string DWGBrowserAddress { get; set; } = Constants.DWGDrawingFinderWebPage;
        #endregion

        #region Icommads
        public ICommand ICRefreshBrowser { get; set; }
        public ICommand ICRefreshDrawingList { get; set; }
        public ICommand ICShow3DRFADownloadedDrawings { get; set; }
        public ICommand ICShow2DRFADownloadedDrawings { get; set; }
        public ICommand ICShowDWGDownloadedDrawings { get; set; }
        public ICommand ICSort { get; set; }
        public ICommand ICView { get; set; }
        public ICommand ICSortByName { get; set; }
        public ICommand ICSortByDate { get; set; }
        public ICommand ICThumbNailView { get; set; }
        public ICommand ICIconView { get; set; }
        public ICommand ICInsertDownloadedDrawing { get; set; }
        public ICommand ICommandGotoContainingFolder { get; set; }
        public ICommand ICommandDeleteDrawing { get; set; }
        #endregion

        #region commands
        /// <summary>
        /// Delete selected drawings
        /// </summary>
        private void DeleteDrawing()
        {
            try
            {
                MessageBoxResult result = MessageBox.Show("Do you want to delete all selected files?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (result == MessageBoxResult.No) return;

                var selectedDrawings = _activeListBox.SelectedItems;
                foreach (var item in selectedDrawings)
                {
                    var drawing = item as DownloadedFile;
                    FileUtility.DeleteFile(drawing.FilePath);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        /// <summary>
        /// Go to containing folder
        /// </summary>
        private void GotoContainingFolder()
        {
            try
            {
                var selectedDrawing = _activeListBox.SelectedItem as DownloadedFile;
                System.Diagnostics.Process.Start("explorer.exe", string.Format("/select,\"{0}\"", selectedDrawing.FilePath));
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Go to containing folder");
            }
        }

        /// <summary>
        /// Insert selected drawing
        /// </summary>
        private void InsertDownloadedDrawing()
        {
            var selectedDrawing = _activeListBox.SelectedItem as DownloadedFile;
            LoadFileToRevit(selectedDrawing.FilePath);
        }

        /// <summary>
        /// Change context menu item visibility when right click
        /// </summary>
        private void ChangeRightClickContextMenuVisibility()
        {
            var SelectedDrawing = _activeListBox.SelectedItems.Count;
            if (SelectedDrawing == 0)
            {
                InsertDownloadedDrawingVisibility = System.Windows.Visibility.Collapsed;
                GotoContainingFolderVisibility = System.Windows.Visibility.Collapsed;
                DeleteDrawingVisibility = System.Windows.Visibility.Collapsed;
                _activeListBox.ContextMenu.Background = Brushes.Transparent;
            }
            else if (SelectedDrawing == 1)
            {
                InsertDownloadedDrawingVisibility = System.Windows.Visibility.Visible;
                GotoContainingFolderVisibility = System.Windows.Visibility.Visible;
                DeleteDrawingVisibility = System.Windows.Visibility.Visible;
                _activeListBox.ContextMenu.Background = Settings.TabNameBackGroundColorBrush;
            }
            else
            {
                InsertDownloadedDrawingVisibility = System.Windows.Visibility.Collapsed;
                GotoContainingFolderVisibility = System.Windows.Visibility.Collapsed;
                DeleteDrawingVisibility = System.Windows.Visibility.Visible;
                _activeListBox.ContextMenu.Background = Settings.TabNameBackGroundColorBrush;
            }

        }

        /// <summary>
        /// Set downloaded drawing list view as icon view
        /// </summary>
        private void IconView()
        {
            ThumbnailVisibility = System.Windows.Visibility.Collapsed;
            IconVisibility = System.Windows.Visibility.Visible;
            UpdateDownloadedFilesImages();
        }

        /// <summary>
        /// Set Downloaded drawing list view as thumbnail view
        /// </summary>
        private void ThumbNailView()
        {
            ThumbnailVisibility = System.Windows.Visibility.Visible;
            IconVisibility = System.Windows.Visibility.Collapsed;
            UpdateDownloadedFilesImages();
        }
        private void UpdateDownloadedFilesImages()
        {
            if (Downloaded3DRFAFolderVisibility == Visibility.Visible && Downloaded3DRFAFolder != null)
            {
                GetDownloadedFilesPreviewImage(Downloaded3DRFAFolder);
            }
            else if (Downloaded2DRFAFolderVisibility == Visibility.Visible && Downloaded2DRFAFolder != null)
            {
                GetDownloadedFilesPreviewImage(Downloaded2DRFAFolder);
            }
            else if (Downloaded2DDWGFolderVisibility == Visibility.Visible && DWGDownloadedFolder != null)
            {
                GetDownloadedFilesPreviewImage(DWGDownloadedFolder);
            }
            UpdateDownloadedFolder();
        }
        /// <summary>
        /// Sort Downloaded drawings list by Name
        /// </summary>
        private void SortByName()
        {
            try
            {
                if (Downloaded3DRFAFolderVisibility == Visibility.Visible)
                {
                    var firstItem = Downloaded3DRFAFilesItemSource.FirstOrDefault();
                    var lastItem = Downloaded3DRFAFilesItemSource.LastOrDefault();
                    if (firstItem != null && lastItem != null)
                    {
                        if (string.Compare(firstItem.FileName, lastItem.FileName) > 0)
                        {
                            Downloaded3DRFAFilesItemSource = new ObservableCollection<DownloadedFile>(Downloaded3DRFAFilesItemSource.OrderBy(p => p.FileName));
                        }
                        else
                        {
                            Downloaded3DRFAFilesItemSource = new ObservableCollection<DownloadedFile>(Downloaded3DRFAFilesItemSource.OrderByDescending(p => p.FileName));

                        }
                    }
                }
                else if (Downloaded2DRFAFolderVisibility == Visibility.Visible)
                {
                    var firstItem = Downloaded2DRFAFilesItemSource.FirstOrDefault();
                    var lastItem = Downloaded2DRFAFilesItemSource.LastOrDefault();
                    if (firstItem != null && lastItem != null)
                    {
                        if (string.Compare(firstItem.FileName, lastItem.FileName) > 0)
                        {
                            Downloaded2DRFAFilesItemSource = new ObservableCollection<DownloadedFile>(Downloaded2DRFAFilesItemSource.OrderBy(p => p.FileName));
                        }
                        else
                        {
                            Downloaded2DRFAFilesItemSource = new ObservableCollection<DownloadedFile>(Downloaded2DRFAFilesItemSource.OrderByDescending(p => p.FileName));

                        }
                    }
                }
                else if (Downloaded2DDWGFolderVisibility == Visibility.Visible)
                {
                    var firstItem = Downloaded2DDWGFilesItemSource.FirstOrDefault();
                    var lastItem = Downloaded2DDWGFilesItemSource.LastOrDefault();
                    if (firstItem != null && lastItem != null)
                    {
                        if (string.Compare(firstItem.FileName, lastItem.FileName) > 0)
                        {
                            Downloaded2DDWGFilesItemSource = new ObservableCollection<DownloadedFile>(Downloaded2DDWGFilesItemSource.OrderBy(p => p.FileName));
                        }
                        else
                        {
                            Downloaded2DDWGFilesItemSource = new ObservableCollection<DownloadedFile>(Downloaded2DDWGFilesItemSource.OrderByDescending(p => p.FileName));

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }

        }

        /// <summary>
        /// Sort Downloaded drawings list by last modified date
        /// </summary>
        private void SortByDate()
        {
            try
            {
                if (Downloaded3DRFAFolderVisibility == Visibility.Visible)
                {
                    var firstItem = Downloaded3DRFAFilesItemSource.FirstOrDefault();
                    var lastItem = Downloaded3DRFAFilesItemSource.LastOrDefault();
                    if (firstItem != null && lastItem != null)
                    {
                        if (firstItem.LastModifiedDate < lastItem.LastModifiedDate)
                        {
                            Downloaded3DRFAFilesItemSource = new ObservableCollection<DownloadedFile>(Downloaded3DRFAFilesItemSource.OrderByDescending(p => p.LastModifiedDate));
                        }
                        else
                        {
                            Downloaded3DRFAFilesItemSource = new ObservableCollection<DownloadedFile>(Downloaded3DRFAFilesItemSource.OrderBy(p => p.LastModifiedDate));

                        }
                    }
                }
                else if (Downloaded2DRFAFolderVisibility == Visibility.Visible)
                {
                    var firstItem = Downloaded2DRFAFilesItemSource.FirstOrDefault();
                    var lastItem = Downloaded2DRFAFilesItemSource.LastOrDefault();
                    if (firstItem != null && lastItem != null)
                    {
                        if (firstItem.LastModifiedDate < lastItem.LastModifiedDate)
                        {
                            Downloaded2DRFAFilesItemSource = new ObservableCollection<DownloadedFile>(Downloaded2DRFAFilesItemSource.OrderByDescending(p => p.LastModifiedDate));
                        }
                        else
                        {
                            Downloaded2DRFAFilesItemSource = new ObservableCollection<DownloadedFile>(Downloaded2DRFAFilesItemSource.OrderBy(p => p.LastModifiedDate));
                        }
                    }
                }
                else if (Downloaded2DDWGFolderVisibility == Visibility.Visible)
                {
                    var firstItem = Downloaded2DDWGFilesItemSource.FirstOrDefault();
                    var lastItem = Downloaded2DDWGFilesItemSource.LastOrDefault();
                    if (firstItem != null && lastItem != null)
                    {
                        if (firstItem.LastModifiedDate < lastItem.LastModifiedDate)
                        {
                            Downloaded2DDWGFilesItemSource = new ObservableCollection<DownloadedFile>(Downloaded2DDWGFilesItemSource.OrderByDescending(p => p.LastModifiedDate));
                        }
                        else
                        {
                            Downloaded2DDWGFilesItemSource = new ObservableCollection<DownloadedFile>(Downloaded2DDWGFilesItemSource.OrderBy(p => p.LastModifiedDate));

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                Log.Error(ex.Message);
            }


        }

        /// <summary>
        /// Action when click on view drop down
        /// </summary>
        private void ViewClick()
        {
            _mainPage.BtView.ContextMenu.IsEnabled = true;
            _mainPage.BtView.ContextMenu.PlacementTarget = _mainPage.BtView;
            _mainPage.BtView.ContextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
            _mainPage.BtView.ContextMenu.IsOpen = true;
        }

        /// <summary>
        /// Action when click on Sort drop down
        /// </summary>
        private void SortClick()
        {
            _mainPage.BtSort.ContextMenu.IsEnabled = true;
            _mainPage.BtSort.ContextMenu.PlacementTarget = _mainPage.BtSort;
            _mainPage.BtSort.ContextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
            _mainPage.BtSort.ContextMenu.IsOpen = true;
        }

        /// <summary>
        /// Show DWG downloaded folder in downloaded drawing list view
        /// </summary>
        public void ShowDWGDownloadedDrawings(bool isReset = false)
        {
            Downloaded3DRFAFolderVisibility = Visibility.Collapsed;
            Downloaded2DRFAFolderVisibility = Visibility.Collapsed;
            Downloaded2DDWGFolderVisibility = Visibility.Visible;
            _activeListBox = _mainPage.ListBox2DDWGDownloadedDrawings;
            // get value for downloaded folder
            if (DWGDownloadedFolder == null || isReset)
            {
                DWGDownloadedFolder = new DownloadedFolder(Settings.DWGDownloadedFolder, ".dwg");
                DWGDownloadedFolder.FileContainFilter = string.Empty;
                DWGDownloadedFolder.GetAllFiles();
            }
            GetDownloadedFilesPreviewImage(DWGDownloadedFolder);
            UpdateDownloadedFolder();
        }

        /// <summary>
        /// Show 3D RFA downloaded folder in downloaded drawing list view
        /// </summary>
        public void Show3DRFADownloadedDrawings(bool isReset = false)
        {
            try
            {
                Downloaded3DRFAFolderVisibility = Visibility.Visible;
                Downloaded2DRFAFolderVisibility = Visibility.Collapsed;
                Downloaded2DDWGFolderVisibility = Visibility.Collapsed;
                _activeListBox = _mainPage.ListBox3DRFADownloadedDrawings;

                // get value for downloaded folder
                if (Downloaded3DRFAFolder == null || isReset)
                {
                    Downloaded3DRFAFolder = new DownloadedFolder(Settings.RFADownloadedFolder, ".rfa");
                    Downloaded3DRFAFolder.FileContainFilter = "_3d_";
                    Downloaded3DRFAFolder.GetAllFiles();

                }
                GetDownloadedFilesPreviewImage(Downloaded3DRFAFolder);
                UpdateDownloadedFolder();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Can not show RFA downloaded folder");
                throw ex;
            }
        }

        /// <summary>
        /// Show 2D RFA downloaded folder in downloaded drawing list view
        /// </summary>
        public void Show2DRFADownloadedDrawings(bool isReset = false)
        {
            try
            {
                Downloaded3DRFAFolderVisibility = Visibility.Collapsed;
                Downloaded2DRFAFolderVisibility = Visibility.Visible;
                Downloaded2DDWGFolderVisibility = Visibility.Collapsed;
                _activeListBox = _mainPage.ListBox2DRFADownloadedDrawings;
                // get value for downloaded folder

                if (Downloaded2DRFAFolder == null || isReset)
                {
                    Downloaded2DRFAFolder = new DownloadedFolder(Settings.RFADownloadedFolder, ".rfa");
                    Downloaded2DRFAFolder.FileContainFilter = "_2do_";
                    Downloaded2DRFAFolder.GetAllFiles();
                }
                GetDownloadedFilesPreviewImage(Downloaded2DRFAFolder);
                UpdateDownloadedFolder();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Can not show RFA downloaded folder");
                throw ex;
            }
        }
        private void GetDownloadedFilesPreviewImage(DownloadedFolder folder)
        {
            if (IconVisibility == Visibility.Visible)
            {
                foreach (var item in folder.DownloadedFiles.Where(p => p.IconImageSource == null))
                {
                    item.GetIcon();
                }
            }
            else if (ThumbnailVisibility == Visibility.Visible)
            {
                foreach (var item in folder.DownloadedFiles.Where(p => p.ThumbailImageSource == null))
                {
                    item.GetThumbnail();
                }
            }
        }

        private System.ComponentModel.BackgroundWorker backgroundWorker1 = new System.ComponentModel.BackgroundWorker();

        public void RefreshDownloadedFolder()
        {
            try
            {
                _mainPage.Dispatcher.Invoke(delegate
                {
                    if (Downloaded3DRFAFolderVisibility == Visibility.Visible)
                    {
                        Show3DRFADownloadedDrawings(true);
                    }
                    else if (Downloaded2DRFAFolderVisibility == Visibility.Visible)
                    {
                        Show2DRFADownloadedDrawings(true);
                    }
                    else if (Downloaded2DDWGFolderVisibility == Visibility.Visible)
                    {
                        ShowDWGDownloadedDrawings(true);
                    }
                });
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw ex;
            }
        }

        /// <summary>
        /// Update list item inside downloaded folder
        /// </summary>
        public void UpdateDownloadedFolder()
        {
            try
            {
                _mainPage.Dispatcher.Invoke(delegate
                {
                    if (Downloaded3DRFAFolderVisibility == Visibility.Visible && Downloaded3DRFAFolder != null)
                    {
                        Downloaded3DRFAFilesItemSource = new ObservableCollection<DownloadedFile>(Downloaded3DRFAFolder.DownloadedFiles);
                    }
                    else if (Downloaded2DRFAFolderVisibility == Visibility.Visible && Downloaded2DRFAFolder != null)
                    {
                        Downloaded2DRFAFilesItemSource = new ObservableCollection<DownloadedFile>(Downloaded2DRFAFolder.DownloadedFiles);
                    }
                    else if (Downloaded2DDWGFolderVisibility == Visibility.Visible && DWGDownloadedFolder != null)
                    {
                        Downloaded2DDWGFilesItemSource = new ObservableCollection<DownloadedFile>(DWGDownloadedFolder.DownloadedFiles);
                    }
                });
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw ex;
            }


        }

        /// <summary>
        /// Refresh drawing finder web browser and DWG web browser
        /// </summary>
        private void RefreshBrowser()
        {
            try
            {
                if (_mainPage.RevitBrowser != null && _mainPage.RevitBrowser.Content != null)
                {
                    _mainPage.RevitBrowser.Reload();
                }
                if (_mainPage.DWGBrowser != null && _mainPage.DWGBrowser.Content != null)
                {
                    _mainPage.DWGBrowser.Reload();
                }
                ButtonWrappanel_SizeChanged(null, null);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw ex;
            }

        }

        /// <summary>
        /// Load a file into revit model
        /// </summary>
        /// <param name="filePath"></param>
        public void LoadFileToRevit(string filePath)
        {
            try
            {
                var fileExt = Path.GetExtension(filePath);
                switch (fileExt)
                {
                    case ".dwg":
                    case ".dxf":
                        MakeRequest(RequestId.LoadDWGFile, filePath);
                        break;
                    case ".rfa":
                        MakeRequest(RequestId.LoadRFAFile, filePath);
                        break;
                    default:
                        var result = MessageBox.Show("This " + fileExt + " file must be placed manually.\n Click ok to go to containing folder.", "Warning!", MessageBoxButton.OKCancel);
                        if (result == MessageBoxResult.OK)
                        {
                            System.Diagnostics.Process.Start("explorer.exe", string.Format("/select,\"{0}\"", filePath));
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw ex;
            }

        }

        /// <summary>
        /// Make a request to revit
        /// </summary>
        /// <param name="request"></param>
        /// <param name="inputString"></param>
        private void MakeRequest(RequestId request, string inputString)
        {
            try
            {
                Handler.Request.Make(request);
                Handler.Request.FilePath = inputString;
                ExEvent.Raise();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw ex;
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// This event excute when mouse is move on main page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Main_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                //drag is heppen 
                //Prepare for Drag and Drop 
                System.Windows.Point mpos = e.GetPosition(null);
                Vector diff = this._startPoint - mpos;

                if (e.LeftButton == MouseButtonState.Pressed &&
                    (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
                    Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
                {
                    //hooking on Mouse Up
                    //InterceptMouse.m_hookID = InterceptMouse.SetHook(InterceptMouse.m_proc);
                    ListBoxItem listBoxItem = FindAnchestor<ListBoxItem>((DependencyObject)e.OriginalSource);
                    if (listBoxItem == null)
                        return;
                    //ataching the event for hadling drop
                    _activeListBox.QueryContinueDrag += queryhandler;
                    //begin drag and drop
                    // Find the data behind the ListViewItem
                    DownloadedFile contact = (DownloadedFile)_activeListBox.ItemContainerGenerator.ItemFromContainer(listBoxItem);
                    // Initialize the drag & drop operation
                    DataObject dragData = new DataObject("myContact", contact);
                    DragDrop.DoDragDrop(listBoxItem, dragData, DragDropEffects.Move);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw ex;
            }
        }

        /// <summary>
        /// Continuosly tracking Dragging mouse position
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DragSourceQueryContinueDrag(object sender, QueryContinueDragEventArgs e)
        {
            try
            {
                //whe keystate is non, draop is heppen
                if (e.KeyStates == DragDropKeyStates.None)
                {
                    //unsubscribe event
                    _activeListBox.QueryContinueDrag -= queryhandler;
                    e.Handled = true;
                    //Unhooking on Mouse Up
                    //InterceptMouse.UnhookWindowsHookEx(InterceptMouse.m_hookID);
                    // get Item
                    var listBox = (ListBox)sender;
                    //if (InterceptMouse.IsMouseOutsideApp)
                    //{
                    DownloadedFile file = (DownloadedFile)listBox.SelectedItem;
                    //notifiy user about drop result
                    if (file != null && IsMouseOutofScreen())
                        LoadFileToRevit(file.FilePath);
                    //}

                }
            }
            catch (Exception ex)
            {
                Log.Information(ex.ToString());
                _mainPage.Dispatcher.BeginInvoke(new Action(() => DragSourceQueryContinueDrag(sender, e)));
            }

        }

        /// <summary>
        /// Determine is point out of screen
        /// </summary>
        /// <returns></returns>
        private bool IsMouseOutofScreen()
        {
            try
            {
                //check if POint in main window
                System.Windows.Point point = InterceptMouse.GetCursorPosition();
                var ptw = DrawingFinderPage.Main.PointFromScreen(point);
                var w = DrawingFinderPage.Main.Width;
                var h = DrawingFinderPage.Main.Height;
                //if point is outside MainWindow
                if (ptw.X < 0 || ptw.Y < 0 || ptw.X > w || ptw.Y > h)
                    return true;
                return false;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                throw ex;
            }
            return true;
        }

        private static T FindAnchestor<T>(DependencyObject current)
           where T : DependencyObject
        {
            do
            {
                if (current is T)
                {
                    return (T)current;
                }
                current = VisualTreeHelper.GetParent(current);
            }
            while (current != null);
            return null;
        }

        /// <summary>
        /// This event excute when mouse left button down on main page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Main_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                _startPoint = e.GetPosition(null);
                HitTestResult r = VisualTreeHelper.HitTest(_mainPage, e.GetPosition(_mainPage));
                if (r != null)
                    if (r.VisualHit.GetType() != typeof(ListBoxItem) && _activeListBox != null)
                    {
                        _activeListBox.UnselectAll();
                    }
            }
            catch (Exception ex)
            {
                Log.Information(ex.ToString());
                throw ex;
            }

        }

        /// <summary>
        /// This event excute when mouse right button down on main page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Main_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (_activeListBox == null)
                {
                    return;
                }
                if (_activeListBox.SelectedItems.Count == 1
                                           || _activeListBox.SelectedItems.Count == 0)
                {
                    _activeListBox.SelectedItems.Clear();
                    var selectItem = FindAnchestor<ListBoxItem>((DependencyObject)e.OriginalSource);
                    if (selectItem != null)
                        _activeListBox.SelectedItems.Add((DownloadedFile)selectItem.DataContext);
                }
                ChangeRightClickContextMenuVisibility();
            }
            catch (Exception ex)
            {
                Log.Information(ex.ToString());
                throw ex;
            }

        }
        private ListBox _activeListBox;
        /// <summary>
        /// This event excute when drawing finder page or downloaded drawing finder page loading state change.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Browser_LoadingStateChanged(object sender, LoadingStateChangedEventArgs e)
        {
            try
            {
                // Update form status when browser loading finish
                if (!e.IsLoading)
                {
                    LoadingStatus = "";
                }
                else
                {
                    LoadingStatus = "Loading...";
                }
            }
            catch (Exception ex)
            {

                Log.Information(ex.ToString());
                throw ex;
            }

        }

        /// <summary>
        /// This event excute before a download start
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnBeforeDownloadFired(object sender, DownloadItem e)
        {
            try
            {
                var myUri = new Uri(e.Url);
                //InitializeDrawingsInDownloadedFolder();
                // Use SuggestedFileName instead of e.URIOrigin file name.
                DownloadedFileFullName = FileUtility.GetDownloadedFilePath(e.SuggestedFileName);
                _processView = new ProcessView();
                _processView.Title = "Download file " + e.SuggestedFileName;
                _processView.Show();
                //// start downloading
                webClient1 = new System.Net.WebClient();
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback += (_sender, certificate, chain, sslPolicyErrors) => true;
                //webClient1.DownloadProgressChanged += WebClient1_DownloadProgressChanged;
                webClient1.DownloadFileCompleted += webClient1_DownloadFileCompleted;
                webClient1.DownloadFileAsync(myUri, DownloadedFileFullName);

            }
            catch (Exception ex)
            {
                Log.Information(ex.ToString());
                throw ex;
            }

        }

        /// <summary>
        /// This event excute when a download started and update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnDownloadUpdatedFired(object sender, DownloadItem e)
        {
            try
            {
                if (_processView != null)
                {
                    _processView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(() => _processView.mBar.Value = e.PercentComplete));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

        }

        /// <summary>
        /// This event excute when a download complete
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void webClient1_DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {

            try
            {
                if (e.Cancelled)
                {
                }
                else
                {
                    _processView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(() => _processView.Close()));
                    UpdateDownloadedFileImgs(DownloadedFileFullName);
                    // if downloaded files is not .ifc we can use dragdrop to load file
                    LoadFileToRevit(DownloadedFileFullName);
                }
            }
            catch (Exception ex)
            {
                Log.Information(ex.ToString());
                _mainPage.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(() => webClient1_DownloadFileCompleted(sender, e)));
            }

        }
        private void UpdateDownloadedFileImgs(string fileFullName)
        {
            if (string.IsNullOrEmpty(fileFullName))
                return;
            if (DWGDownloadedFolder != null)
            {
                var dwgFile = DWGDownloadedFolder.DownloadedFiles.FirstOrDefault(p => p.FilePath == fileFullName);
                if (dwgFile != null)
                {
                    dwgFile.GetThumbnail();
                    dwgFile.GetIcon();
                    UpdateDownloadedFolder();
                }
            }
            if (Downloaded3DRFAFolder != null)
            {
                var rfa3DFile = Downloaded3DRFAFolder.DownloadedFiles.FirstOrDefault(p => p.FilePath == fileFullName);
                if (rfa3DFile != null)
                {
                    rfa3DFile.GetThumbnail();
                    rfa3DFile.GetIcon();
                    UpdateDownloadedFolder();
                }
            }
            if (Downloaded2DRFAFolder != null)
            {
                var rfa2DFile = Downloaded2DRFAFolder.DownloadedFiles.FirstOrDefault(p => p.FilePath == fileFullName);
                if (rfa2DFile != null)
                {
                    rfa2DFile.GetThumbnail();
                    rfa2DFile.GetIcon();
                    UpdateDownloadedFolder();
                }
            }

        }
        /// <summary>
        /// Scale transform in revit 2017
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _mainPage_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DFRApplication.thisApp.Revit_Version < 2018)
                {
                    Matrix m = PresentationSource.FromVisual(_mainPage).CompositionTarget.TransformToDevice;
                    ScaleTransform dpiTransform = new ScaleTransform(1 / m.M11, 1 / m.M22);
                    if (dpiTransform.CanFreeze)
                        dpiTransform.Freeze();
                    _mainPage.LayoutTransform = dpiTransform;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Update length of spacing between button on top
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonWrappanel_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                _mainPage.Dispatcher.Invoke(delegate
                 {
                     if (sender != null)
                     {

                         var panel = (WrapPanel)sender;
                         var maxWidth = panel.ActualWidth - _mainPage.TBBrowserMode.ActualWidth - _mainPage.BRefresh.ActualWidth - _mainPage.B3DRFA.ActualWidth - _mainPage.B2DRFA.ActualWidth - _mainPage.B2DDWG.ActualWidth - _mainPage.BtSort.ActualWidth - _mainPage.BtView.ActualWidth - 30; //30 is total margin
                         _mainPage.Spacing.Width = Math.Max(maxWidth, 0);
                     }
                 });
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                throw ex;
            }

        }
        #endregion
    }
}
