﻿using System;
using System.Windows.Input;

namespace SSTVN.DFR.RV2019
{
    /// <summary>
    /// Basic command that runs an action
    /// </summary>
    public class RelayCommand : ICommand
    {

        #region private members

        /// <summary>
        /// An action to run
        /// </summary>
        private Action mAction;
        #endregion

        #region Events
        /// <summary>
        /// The event thats fired when the <see cref="CanExecute(object)"/> value changed
        /// </summary>
        public event EventHandler CanExecuteChanged = (sender, e) => { };

        #endregion

        #region Contructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public RelayCommand(Action action)
        {
            mAction = action;
        }

        #endregion

        #region Command methods

        /// <summary>
        /// A relay command can always execute
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public bool CanExecute(object parameter)
        {
            return true;
        }

        /// <summary>
        /// Executes the commands Action
        /// </summary>
        /// <param name="parameter"></param>
        public void Execute(object parameter)
        {
            mAction();
        }
        #endregion
    }
}
