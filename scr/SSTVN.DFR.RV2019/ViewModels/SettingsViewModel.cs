﻿using Serilog;
using SSTVN.DFR.RV2019.Model;
using SSTVN.DFR.RV2019.Views;
using System;
using System.IO;
using System.Windows;
using System.Windows.Input;

namespace SSTVN.DFR.RV2019.ViewModels
{
    public class SettingsViewModel : BaseViewModel
    {
        #region private
        private SettingView _view;
        private System.Diagnostics.Stopwatch _Watch = new System.Diagnostics.Stopwatch();
        private bool isHideDrawingFinder;
        private bool isUnhideDrawingFinder;
        private bool isKeepHideDrawingFinder;
        #endregion
        #region Constructor
        public SettingsViewModel(SettingView view)
        {
            _view = view;
            RFADownloadedLocation = Settings.RFADownloadedFolder;
            DWGDownloadedLocation = Settings.DWGDownloadedFolder;
            IsHideDrawingFinder = Settings.IsHideDrawingFinder;
            IsKeepHideDrawingFinder = Settings.IsKeepHideDrawingFinderAfterPlacing;
            IsUnhideDrawingFinder = !Settings.IsKeepHideDrawingFinderAfterPlacing;

            _view.KeyUp += _view_KeyUp;

            ICRFABrowserFolder = new RelayCommand(() => CRFABrowserFolder());
            ICDWGBrowserFolder = new RelayCommand(() => CDWGBrowserFolder());

            ICSetDefault = new RelayCommand(() => CSetDefault());
            ICOk = new RelayCommand(() => COk());
            ICCancel = new RelayCommand(() => CCancel());
        }

        #endregion

        #region Properties
        public string RFADownloadedLocation { get; set; }
        public string DWGDownloadedLocation { get; set; }

        public string SettingIcon { get; set; } = FileUtility.GetApplicationResourcesPath() + "/Icons/settings.png";
        public string FolderIcon { get; set; } = FileUtility.GetApplicationResourcesPath() + "/Icons/Folder_yellow_icon24.png";
        public bool IsHideDrawingFinder
        {
            get => isHideDrawingFinder;
            set => isHideDrawingFinder = value;
        }
        public bool IsUnhideDrawingFinder
        {
            get => isUnhideDrawingFinder;
            set => isUnhideDrawingFinder = value;
        }
        public bool IsKeepHideDrawingFinder
        {
            get => isKeepHideDrawingFinder;
            set => isKeepHideDrawingFinder = value;
        }
        public ICommand ICRFABrowserFolder { get; set; }
        public ICommand ICDWGBrowserFolder { get; set; }
        public ICommand ICSetDefault { get; set; }
        public ICommand ICOk { get; set; }
        public ICommand ICCancel { get; set; }
        #endregion

        #region Events

        /// <summary>
        /// Raise run mode switcher view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _view_KeyUp(object sender, KeyEventArgs e)
        {
            if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.R)
            {
                if (_Watch.IsRunning) return;

                _Watch.Restart();

            }
            else if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.A)
            {
                if (!_Watch.IsRunning) return;

                _Watch.Stop();

                if (_Watch.ElapsedMilliseconds >= 5000)
                {
                    var frm = new SwitchModeView();
                    var diaresult = frm.ShowDialog();
                    if (diaresult == true)
                    {
                        var mainVM = (DrawingFinderVM)DFRApplication.thisApp.DrawingFinderView.DataContext;
                        mainVM.RevitBrowserAddress = Settings.RevitLink;
                        mainVM.BrowserMode = Settings.IsTestingMode ? "Testing Mode" : "";
                    }
                }
                else
                {
                    _Watch.Reset();
                }
            }
        }
        #endregion

        #region Commands
        private void COk()
        {
            try
            {
                if (Settings.RFADownloadedFolder != RFADownloadedLocation || Settings.DWGDownloadedFolder != DWGDownloadedLocation)
                {
                    Settings.RFADownloadedFolder = RFADownloadedLocation;
                    Settings.DWGDownloadedFolder = DWGDownloadedLocation;
                    Settings.IsHideDrawingFinder = IsHideDrawingFinder;
                    Settings.IsKeepHideDrawingFinderAfterPlacing = IsKeepHideDrawingFinder;
                    DrawingFinderPage.Main.Dispatcher.Invoke(delegate
                    {
                        // update downloaded folder
                        var vm = DrawingFinderPage.Main.DataContext as DrawingFinderVM;
                        if (vm != null)
                        {
                            if (vm.Downloaded3DRFAFolder != null)
                            {
                                vm.Downloaded3DRFAFolder.Dispose();
                                vm.Downloaded3DRFAFolder = null;
                            }
                            if (vm.Downloaded2DRFAFolder != null)
                            {
                                vm.Downloaded2DRFAFolder.Dispose();
                                vm.Downloaded2DRFAFolder = null;
                            }
                            if (vm.DWGDownloadedFolder != null)
                            {
                                vm.DWGDownloadedFolder.Dispose();
                                vm.DWGDownloadedFolder = null;
                            }

                            if (vm.Downloaded3DRFAFolderVisibility == Visibility.Visible)
                            {
                                vm.Show3DRFADownloadedDrawings(true);
                            }
                            else if (vm.Downloaded2DRFAFolderVisibility == Visibility.Visible)
                            {
                                vm.Show2DRFADownloadedDrawings(true);
                            }
                            else if (vm.Downloaded2DDWGFolderVisibility == Visibility.Visible)
                            {
                                vm.ShowDWGDownloadedDrawings(true);
                            }
                        }
                    });
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "can not update downloaded folder");
                throw ex;
            }
            _view.Close();
        }

        private void CSetDefault()
        {
            IsHideDrawingFinder = true;

            try
            {
                System.IO.Directory.CreateDirectory(Settings.DefaultRFADownloadedFolder);
                RFADownloadedLocation = Settings.DefaultRFADownloadedFolder;
                DWGDownloadedLocation = Settings.DefaultDWGDownloadedFolder;
                IsHideDrawingFinder = true;
                IsKeepHideDrawingFinder = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Can not set settings to default");
                throw ex;
            }

        }

        private void CCancel()
        {
            _view.Close();
        }

        /// <summary>
        /// Browser for RFA downloaded folder
        /// </summary>
        private void CRFABrowserFolder()
        {
            try
            {
                var folderPath = FileUtility.BrowserForAFolder(RFADownloadedLocation);
                if (folderPath != null)
                {
                    RFADownloadedLocation = Path.Combine(folderPath, "Drawings");
                    if (!Directory.Exists(RFADownloadedLocation))
                    {
                        Directory.CreateDirectory(RFADownloadedLocation);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw ex;
            }
        }

        /// <summary>
        /// Browser for DWG downloaded folder
        /// </summary>
        private void CDWGBrowserFolder()
        {
            try
            {
                var folderPath = FileUtility.BrowserForAFolder(DWGDownloadedLocation);
                if (folderPath != null)
                {
                    DWGDownloadedLocation = Path.Combine(folderPath, "Drawings");
                    if (!Directory.Exists(DWGDownloadedLocation))
                    {
                        Directory.CreateDirectory(DWGDownloadedLocation);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw ex;
            }
        }
        #endregion
    }
}
