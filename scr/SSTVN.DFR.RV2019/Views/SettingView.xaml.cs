﻿using SSTVN.DFR.RV2019.ViewModels;
using System.Windows;

namespace SSTVN.DFR.RV2019.Views
{
    /// <summary>
    /// Interaction logic for SettingView.xaml
    /// </summary>
    public partial class SettingView : Window
    {
        public SettingView()
        {
            InitializeComponent();
            this.DataContext = new SettingsViewModel(this);
        }

    }
}
