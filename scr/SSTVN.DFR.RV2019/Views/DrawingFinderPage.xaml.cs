﻿using Autodesk.Revit.UI;
using Serilog;
using SSTVN.DFR.RV2019.ViewModels;
using System;
using System.Windows;
using System.Windows.Controls;

namespace SSTVN.DFR.RV2019
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class DrawingFinderPage : Page, Autodesk.Revit.UI.IDockablePaneProvider
    {
        /// <summary>
        /// In this sample, the dialog owns the value of the request but it is not necessary. It may as
        /// well be a static property of the application.
        /// </summary>
        public static DrawingFinderPage Main;
        public DrawingFinderPage(ExternalEvent exEvent, RequestHandler handler)
        {
            InitializeComponent();
            Main = this;

            this.DataContext = new DrawingFinderVM(this, exEvent, handler);
            this.Unloaded += DrawingFinderPage_Unloaded;
        }


        private void DrawingFinderPage_Unloaded(object sender, RoutedEventArgs e)
        {
            try
            {

                //Cef.Shutdown();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }

        #region UI State

        #endregion
        #region UI Support

        //private childItem FindVisualChild<childItem>(DependencyObject obj) where childItem : DependencyObject
        //{
        //    for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
        //    {
        //        DependencyObject child = VisualTreeHelper.GetChild(obj, i);
        //        if (child != null && child is childItem) return (childItem)child;
        //        else
        //        {

        //            childItem childOfChild = FindVisualChild<childItem>(child);
        //            if (childOfChild != null) return childOfChild;
        //        }
        //    }

        //    return null;
        //}
        #endregion

        /// <summary>
        /// Called by Revit to initialize dockable pane settings set in DockingSetupDialog.
        /// </summary>
        /// <param name="data"></param>
        public void SetupDockablePane(Autodesk.Revit.UI.DockablePaneProviderData data)
        {
            data.FrameworkElement = this as FrameworkElement;
            DockablePaneProviderData d = new DockablePaneProviderData();
            data.InitialState = new Autodesk.Revit.UI.DockablePaneState();
            data.InitialState.DockPosition = m_position;
            DockablePaneId targetPane;
            if (m_targetGuid == Guid.Empty)
                targetPane = null;
            else targetPane = new DockablePaneId(m_targetGuid);
            if (m_position == DockPosition.Tabbed)
                data.InitialState.TabBehind = targetPane;


            if (m_position == DockPosition.Floating)
            {
                //data.InitialState.SetFloatingRectangle(new Autodesk.Revit.UI.Rectangle(m_left, m_top, m_right, m_bottom));// for revit 2014
                data.InitialState.SetFloatingRectangle(new Autodesk.Revit.DB.Rectangle(m_left, m_top, m_right, m_bottom));
            }
        }


        public void SetInitialDockingParameters(int left, int right, int top, int bottom, DockPosition position, Guid targetGuid)
        {
            m_position = position;
            m_left = left;
            m_right = right;
            m_top = top;
            m_bottom = bottom;
            m_targetGuid = targetGuid;
        }
        #region Data

        private Guid m_targetGuid;
        private DockPosition m_position = DockPosition.Left;
        private int m_left = 1;
        private int m_right = 1;
        private int m_top = 1;
        private int m_bottom = 1;

        #endregion

        private void Button_Click(object sender, RoutedEventArgs e)
        {
        }
    }
}
