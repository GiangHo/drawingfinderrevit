﻿using SSTVN.DFR.RV2019.ViewModels;
using System.Windows;

namespace SSTVN.DFR.RV2019.Views
{
    /// <summary>
    /// Interaction logic for SwitchModeView.xaml
    /// </summary>
    public partial class SwitchModeView : Window
    {
        public SwitchModeView()
        {
            InitializeComponent();
            this.DataContext = new SwitchModeViewModel(this);
        }
    }
}
