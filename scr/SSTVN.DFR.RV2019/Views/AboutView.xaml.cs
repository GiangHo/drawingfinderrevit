﻿using SSTVN.DFR.RV2019.ViewModels;
using System.Windows;

namespace SSTVN.DFR.RV2019.Views
{
    /// <summary>
    /// Interaction logic for About.xaml
    /// </summary>
    public partial class AboutView : Window
    {
        public AboutView()
        {
            InitializeComponent();
            this.DataContext = new AboutViewModel(this);
        }
    }
}
