﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace SSTVN.DFR.RV2019
{
    /// <summary>
    ///   A class with methods to execute requests made by the dialog user.
    /// </summary>
    /// 
    public class RequestHandler : IExternalEventHandler
    {
        // The value of the latest request made by the modeless form 
        private Request m_request = new Request();

        /// <summary>
        /// A public property to access the current request value
        /// </summary>
        public Request Request
        {
            get { return m_request; }
        }

        /// <summary>
        ///   A method to identify this External Event Handler
        /// </summary>
        public String GetName()
        {
            return "R2020 External Event Sample";
        }

        /// <summary>
        ///   The top method of the event handler.
        /// </summary>
        /// <remarks>
        ///   This is called by Revit after the corresponding
        ///   external event was raised (by the modeless form)
        ///   and Revit reached the time at which it could call
        ///   the event's handler (i.e. this object)
        /// </remarks>
        /// 
        public void Execute(UIApplication uiapp)
        {
            try
            {
                switch (Request.Take())
                {
                    case RequestId.None:
                        {
                            return;  // no request at this time -> we can leave immediately
                        }
                    case RequestId.LoadRFAFile:
                        {
                            PlaceFamily(uiapp);
                            break;
                        }
                    case RequestId.LoadDWGFile:
                        {
                            placeDWG(uiapp);
                            break;
                        }
                    default:
                        {
                            // some kind of a warning here should
                            // notify us about an unexpected request 
                            break;
                        }
                }
            }
            finally
            {
            }

            return;
        }

        /// <summary>
        /// Place a DWG drawing into current model
        /// </summary>
        /// <param name="uiapp"></param>
        private void placeDWG(UIApplication uiapp)
        {
            MoveMouseToActiveView(uiapp);
            List<string> a = new List<string>();
            a.Add(Request.FilePath);
            UIApplication.DoDragDrop(a);
        }

        /// <summary>
        /// Detect mouse position , move mouse position into current view if current possition is outsize of this
        /// </summary>
        /// <param name="uiapp"></param>
        private void MoveMouseToActiveView(UIApplication uiapp)
        {
            try
            {
                Log.Information("Start mouse move");
                #region get active UI view rectangle
                UIDocument uidoc = uiapp.ActiveUIDocument;
                Document doc = uidoc.Document;
                View view = doc.ActiveView;
                IList<UIView> uiviews = uidoc.GetOpenUIViews();
                UIView uiview = null;
                if (uiviews.Count <= 0)
                    return;
                foreach (UIView uv in uiviews)
                {
                    if (uv.ViewId.Equals(view.Id))
                    {
                        uiview = uv;
                        break;
                    }
                }
                // Get active view rectange
                Rectangle rectangle = uiview.GetWindowRectangle();
                // convert active view rectangle to Rect
                var rect = new System.Windows.Rect(rectangle.Left, rectangle.Top, rectangle.Right - rectangle.Left, rectangle.Bottom - rectangle.Top);
                #endregion

                // get current mouse point
                var curMousePoint = InterceptMouse.GetCursorPosition();
                int X = 0;
                int Y = 0;
                #region Find point outside docpanel
                // Check current point is inside of panel
                //check if POint in main window
                var ptw = DrawingFinderPage.Main.PointFromScreen(curMousePoint);
                var w = DrawingFinderPage.Main.Width;
                var h = DrawingFinderPage.Main.Height;
                //if point is inside MainWindow
                if (!(ptw.X < 0 || ptw.Y < 0 || ptw.X > w || ptw.Y > h))
                {
                    FindInsertionPoint(rectangle.Left, rectangle.Right, rectangle.Top, rectangle.Bottom, out X, out Y);
                    InterceptMouse.SetCursorPos(X, Y);
                }

                // 
                #endregion
                // Move mouse to center of active view when current point is outsize of active view
                if (!rect.Contains(curMousePoint))
                    InterceptMouse.SetCursorPos(X, Y);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);

            }

        }

        /// <summary>
        /// Find posible insertion point
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <param name="top"></param>
        /// <param name="bot"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        private void FindInsertionPoint(int left, int right, int top, int bot, out int x, out int y)
        {
            var w = DrawingFinderPage.Main.Width;
            var h = DrawingFinderPage.Main.Height;
            //check if Point 1 in main window
            var xCenter = (left + right) / 2;
            var yCenter = (top + bot) / 2;

            // check if Point 1 in main window
            System.Windows.Point pt = new System.Windows.Point(xCenter, yCenter);
            var ptw = DrawingFinderPage.Main.PointFromScreen(pt);
            //if point is outside dockpanel
            if (ptw.X < 0 || ptw.Y < 0 || ptw.X > w || ptw.Y > h)
            {
                x = xCenter;
                y = yCenter;
                return;
            }
            // determine which part of rectangle
            if (ptw.X < w / 2 && ptw.Y < h / 2)
            {
                FindInsertionPoint(left, xCenter, top, yCenter, out x, out y);
            }
            else if (ptw.X < w / 2 && ptw.Y >= h / 2)
            {
                FindInsertionPoint(left, xCenter, yCenter, bot, out x, out y);
            }
            else if (ptw.X >= w / 2 && ptw.Y < h / 2)
            {
                FindInsertionPoint(xCenter, right, top, yCenter, out x, out y);
            }
            else
            {
                FindInsertionPoint(xCenter, right, yCenter, bot, out x, out y);
            }
        }
        private void PlaceFamily(UIApplication uiapp)
        {
            var revitFileInfo = new FileInfo(Request.FilePath);
            try
            {
                MoveMouseToActiveView(uiapp);

                var activeDoc = uiapp.ActiveUIDocument.Document;
                var familyName = Path.GetFileNameWithoutExtension(revitFileInfo.Name);
                FilteredElementCollector familyCollector = new FilteredElementCollector(activeDoc).OfClass(typeof(Family));
                Family thisFamily = null;
                foreach (Family family in familyCollector)
                {
                    //To search by FamilySymbol name
                    if (family.Name == familyName)
                    {
                        thisFamily = family;
                        break;
                    }

                }
                using (Transaction tx = new Transaction(activeDoc))
                {
                    tx.Start("Load Family");
                    if (thisFamily == null)
                    {
                        ///  error in this method
                        if (!activeDoc.LoadFamily(revitFileInfo.FullName, out thisFamily))
                        {

                            TaskDialog.Show("revit", "unable to load " + revitFileInfo.Name);
                        }

                    }
                    tx.Commit();
                }
                //uiapp.OpenAndActivateDocument(Request.m_request_string);
                if (thisFamily != null)
                {
                    var familysymbolids = thisFamily.GetFamilySymbolIds();
                    List<string> a = new List<string>();
                    a.Add(Request.FilePath);
                    switch (uiapp.ActiveUIDocument.ActiveView.ViewType)
                    {
                        case ViewType.Undefined:
                            UIApplication.DoDragDrop(a);
                            break;
                        case ViewType.FloorPlan:
                            foreach (ElementId id in familysymbolids)
                            {
                                var symbol = thisFamily.Document.GetElement(id) as FamilySymbol;
                                var symbolId = symbol.Id;
                                var element = activeDoc.GetElement(symbolId);
                                uiapp.ActiveUIDocument.PostRequestForElementTypePlacement((ElementType)element);
                            }
                            break;
                        case ViewType.EngineeringPlan:
                        case ViewType.AreaPlan:
                        case ViewType.CeilingPlan:
                        case ViewType.Elevation:
                        case ViewType.Section:
                        case ViewType.Detail:
                            UIApplication.DoDragDrop(a);
                            break;
                        case ViewType.ThreeD:
                            foreach (ElementId id in familysymbolids)
                            {
                                var symbol = thisFamily.Document.GetElement(id) as FamilySymbol;
                                var symbolId = symbol.Id;
                                var element = activeDoc.GetElement(symbolId);
                                uiapp.ActiveUIDocument.PostRequestForElementTypePlacement((ElementType)element);
                            }
                            break;
                        case ViewType.Schedule:
                        case ViewType.DraftingView:
                        case ViewType.DrawingSheet:
                        case ViewType.Legend:
                        case ViewType.Report:
                        case ViewType.ProjectBrowser:
                        case ViewType.SystemBrowser:
                        case ViewType.CostReport:
                        case ViewType.LoadsReport:
                        case ViewType.PresureLossReport:
                        case ViewType.PanelSchedule:
                        case ViewType.ColumnSchedule:
                        case ViewType.Walkthrough:
                        case ViewType.Rendering:
                        case ViewType.Internal:
                            UIApplication.DoDragDrop(a);
                            break;
                        default:
                            break;
                    }

                }
            }
            catch (System.Exception ex)
            {

                throw ex;
            }
        }

    }  // class
}  // namespace
