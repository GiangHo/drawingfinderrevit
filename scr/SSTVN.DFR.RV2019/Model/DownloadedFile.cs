﻿using Serilog;
using System;
using System.Drawing;
using System.IO;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media.Imaging;

namespace SSTVN.DFR.RV2019
{

    /// <summary>
    /// The Downloaded file model
    /// </summary>
    public class DownloadedFile
    {
        #region Constructors

        /// <summary>
        /// The constructor initialize an file information ready for download
        /// </summary>
        /// <param name="filePath"></param>
        public DownloadedFile(string filePath)
        {
            FilePath = filePath;
            NameWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(filePath);
            FileName = Path.GetFileName(FilePath);
            FileInfo ThisFileInfo = new FileInfo(this.FilePath);
            LastModifiedDate = ThisFileInfo.LastWriteTime;
        }

        public void GetThumbnail()
        {
            try
            {
                var a = DrawingFinderPage.Main;
                if (a != null)
                {
                    DrawingFinderPage.Main.Dispatcher.Invoke(delegate
                    {
                        // Init thumbnail image
                        var image = ThumbnailGetter.WindowsThumbnailProvider.GetThumbnail(FilePath, 256, 256, ThumbnailGetter.ThumbnailOptions.None);
                        Bitmap bitmap = new System.Drawing.Bitmap(image);//it is in the memory now 
                        ThumbailImageSource = Imaging.CreateBitmapSourceFromHBitmap(bitmap.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                        bitmap.Dispose();
                        image.Dispose();
                    });
                }
            }
            catch
            {
                Log.Information("Can not get thumbnail of " + FilePath);
            }
        }
        public void GetIcon()
        {
            try
            {
                var a = DrawingFinderPage.Main;
                if (a != null)
                {
                    DrawingFinderPage.Main.Dispatcher.Invoke(delegate
                    {
                        // Init Icon Image
                        var image = ThumbnailGetter.WindowsThumbnailProvider.GetThumbnail(FilePath, 24, 24, ThumbnailGetter.ThumbnailOptions.IconOnly);
                        Bitmap bitmap = new System.Drawing.Bitmap(image);//it is in the memory now 
                        IconImageSource = Imaging.CreateBitmapSourceFromHBitmap(bitmap.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                        bitmap.Dispose();
                        image.Dispose();
                    });
                }
            }
            catch
            {
                Log.Information("Can not get thumbnail of " + FilePath);
            }
        }
        #endregion

        #region properties
        /// <summary>
        /// The file name property
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// The file full name property
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        /// The Last time modified time property
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// File Thumbnail
        /// </summary>
        public BitmapSource ThumbailImageSource { get; set; }

        /// <summary>
        /// File Icon
        /// </summary>
        public BitmapSource IconImageSource { get; set; }

        /// <summary>
        /// File Name without extension
        /// </summary>
        public string NameWithoutExtension { get; set; }
        #endregion
    }
}
