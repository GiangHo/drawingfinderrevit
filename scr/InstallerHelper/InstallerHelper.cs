﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration.Install;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace InstallerHelper
{
    /// <summary>
    /// This installer class overrides the base installer class to provide
    /// additional functionality for the Custom Actions.
    /// </summary>
    [RunInstaller(true)]
    public class InstallerHelper : Installer
    {
        static void Main()
        { }
        /// <summary>
        /// Constructor method
        /// </summary>
        public InstallerHelper()
            : base()
        {
        }

        public override void Install(IDictionary stateSaver)
        {
            var p = Process.GetProcessesByName("Revit").FirstOrDefault();

            if (p != null)
                throw new InstallException("Please close all Revit windows and try agian!");

            base.Install(stateSaver);
        }

        protected override void OnAfterInstall(IDictionary savedState)
        {
            base.OnAfterInstall(savedState);
            try
            {
                string addInFilePath = InstallFolder() + @"\Simpson Strong-Tie\Drawing Finder for Revit\Revit Res\DrawingFinderForRevit.addin";
                var fileInfo = new FileInfo(addInFilePath);
                var revit2017Folder = @"C:\ProgramData\Autodesk\Revit\Addins\2017";
                var revit2018Folder = @"C:\ProgramData\Autodesk\Revit\Addins\2018";
                var revit2020Folder = @"C:\ProgramData\Autodesk\Revit\Addins\2020";
                var revit2021Folder = @"C:\ProgramData\Autodesk\Revit\Addins\2021";
                if (Directory.Exists(revit2017Folder))
                    fileInfo.CopyTo(revit2017Folder + @"\DrawingFinderForRevit.addin", true);
                if (Directory.Exists(revit2018Folder))
                    fileInfo.CopyTo(revit2018Folder + @"\DrawingFinderForRevit.addin", true);
                if (Directory.Exists(revit2020Folder))
                    fileInfo.CopyTo(revit2020Folder + @"\DrawingFinderForRevit.addin", true);
                if (Directory.Exists(revit2021Folder))
                    fileInfo.CopyTo(revit2021Folder + @"\DrawingFinderForRevit.addin", true);

            }
            catch
            {
            }
        }
        private static string InstallFolder()
        {
            if (8 == IntPtr.Size || (!String.IsNullOrEmpty(Environment.GetEnvironmentVariable("PROCESSOR_ARCHITEW6432"))))
            {
                return Environment.GetEnvironmentVariable("ProgramFiles(x86)");
            }

            return Environment.GetEnvironmentVariable("ProgramFiles");
        }

        protected override void OnBeforeUninstall(IDictionary savedState)
        {
            base.OnBeforeUninstall(savedState);
            try
            {
                var revit2017Folder = @"C:\ProgramData\Autodesk\Revit\Addins\2017";
                var revit2018Folder = @"C:\ProgramData\Autodesk\Revit\Addins\2018";
                var revit2020Folder = @"C:\ProgramData\Autodesk\Revit\Addins\2020";
                var revit2021Folder = @"C:\ProgramData\Autodesk\Revit\Addins\2021";
                if (Directory.Exists(revit2017Folder))
                    File.Delete(revit2017Folder + @"\DrawingFinderForRevit.addin");
                if (Directory.Exists(revit2018Folder))
                    File.Delete(revit2018Folder + @"\DrawingFinderForRevit.addin");
                if (Directory.Exists(revit2020Folder))
                    File.Delete(revit2020Folder + @"\DrawingFinderForRevit.addin");
                if (Directory.Exists(revit2021Folder))
                    File.Delete(revit2021Folder + @"\DrawingFinderForRevit.addin");
            }
            catch
            {
            }
        }

    }
}
