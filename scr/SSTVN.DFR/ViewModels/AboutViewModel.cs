﻿using SSTVN.DFR.Views;
using System;
using System.Drawing;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace SSTVN.DFR.ViewModels
{
    public class AboutViewModel : BaseViewModel
    {
        #region private
        private AboutView _view;
        #endregion

        #region Constructor
        public AboutViewModel(AboutView view)
        {
            _view = view;
            var icon = SystemIcons.Information;
            Bitmap bitmap = icon.ToBitmap();
            IntPtr hBitmap = bitmap.GetHbitmap();

            ImageSource wpfBitmap =
                 Imaging.CreateBitmapSourceFromHBitmap(
                      hBitmap, IntPtr.Zero, Int32Rect.Empty,
                      BitmapSizeOptions.FromEmptyOptions());
            WindowIcon = wpfBitmap;

            ICGotoWebLink = new RelayCommand(() => CGotoWebLink());
            ICCheckForUpdate = new RelayCommand(() => CCheckForUpdate());
            ICOk = new RelayCommand(() => COk());
        }
        #endregion

        #region Properties
        public ImageSource WindowIcon { get; set; }
        public string AppIconImageSource { get; set; } = FileUtility.GetApplicationResourcesPath() + "/Icons/SimpsonLogo72.jpg";
        public string DFRLogo { get; set; } = FileUtility.GetApplicationResourcesPath() + "/Icons/DFRIcon.jpg";
        public string Version
        {
            get
            {
                var version = Assembly.GetExecutingAssembly().GetName().Version;
                return string.Format("Version: {0}.{1}.{2}", version.Major, version.Minor, version.Build);
            }
        }
        public string CopyRight
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
            }
        }
        public string WebLink
        {
            get
            {
                return "http://www.strongtie.com";
            }
        }
        public ICommand ICGotoWebLink { get; set; }
        public ICommand ICCheckForUpdate { get; set; }
        public ICommand ICOk { get; set; }
        #endregion

        #region Commands
        private void CGotoWebLink()
        {
            System.Diagnostics.Process.Start(WebLink);
        }
        private void CCheckForUpdate()
        {
            System.Diagnostics.Process.Start(Constants.PluginIntroductionWebPage);
        }
        private void COk()
        {
            _view.Close();
        }
        #endregion
    }
}
