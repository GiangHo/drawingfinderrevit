﻿using SSTVN.DFR.Views;
using System.Windows.Forms;
using System.Windows.Input;

namespace SSTVN.DFR.ViewModels
{
    /// <summary>
    /// Switch default mode to testing mode view model
    /// </summary>
    public class SwitchModeViewModel : BaseViewModel
    {
        #region Private
        private SwitchModeView _view;

        #endregion

        #region Constructor
        public SwitchModeViewModel(SwitchModeView view)
        {
            _view = view;
            if (Settings.IsTestingMode)
            {
                IsTesting = true;
                IsDefault = false;
            }
            else
            {
                IsTesting = false;
                IsDefault = true;
            }

            TestLink = Settings.TestingLink;

            ICOk = new RelayCommand(() => COk());
            ICCancel = new RelayCommand(() => CCancel());
        }
        #endregion

        #region Properties
        public bool IsTesting { get; set; }
        public bool IsDefault { get; set; }
        public string TestLink { get; set; }

        public ICommand ICOk { get; set; }
        public ICommand ICCancel { get; set; }
        #endregion

        #region Commands
        private void COk()
        {
            Settings.TestingLink = TestLink;
            Settings.IsTestingMode = IsTesting;
            _view.DialogResult = true;
            _view.Close();
        }

        private void CCancel()
        {
            _view.Close();
        }
        #endregion

    }
}
