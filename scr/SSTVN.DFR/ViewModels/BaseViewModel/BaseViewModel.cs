﻿using System.ComponentModel;

namespace SSTVN.DFR
{

    /// <summary>
    /// A base View model that fires that Property changed that needed
    /// </summary>
    public class BaseViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// The Event that is fired when any child property its value
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged = (sender, e) => { };

        public void OnpropertyChanged(string name)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }
}
