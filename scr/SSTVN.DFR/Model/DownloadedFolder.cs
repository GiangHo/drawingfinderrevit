﻿using FileWatcherEx;
using Serilog;
using SSTVN.DFR.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Documents;

namespace SSTVN.DFR.Model
{
    public class DownloadedFolder
    {
        private FileWatcherEx.FileWatcherEx Fw = new FileWatcherEx.FileWatcherEx();
        private string _folderPath;
        private string _fileExtension;
        public DownloadedFolder(string folderPath, string fileExt)
        {
            _folderPath = folderPath;
            _fileExtension = fileExt;

            Fw = new FileWatcherEx.FileWatcherEx(folderPath);
            Fw.OnRenamed += _fw_OnRenamed;
            Fw.OnCreated += _fw_OnCreated;
            Fw.OnDeleted += _fw_OnDeleted;
            Fw.OnChanged += _fw_OnChanged;
            Fw.OnError += _fw_OnError;
            Fw.Start();
        }

        private void _fw_OnError(object sender, ErrorEventArgs e)
        {
        }

        private void _fw_OnChanged(object sender, FileChangedEvent e)
        {
        }

        private void _fw_OnDeleted(object sender, FileChangedEvent e)
        {
            var file = DownloadedFiles.FirstOrDefault(f => f.FilePath == e.FullPath);
            if (file != null)
            {
                DownloadedFiles.Remove(file);
                switch (Path.GetExtension(e.FullPath))
                {
                    default:
                        break;
                }
                FolderUpdate();
            }

        }

        private void _fw_OnCreated(object sender, FileChangedEvent e)
        {
            var file = new DownloadedFile(e.FullPath);
            if (file != null && Path.GetExtension(file.FileName) == _fileExtension
            && file.FileName.ToLower().Contains(FileContainFilter))
            {
                Thread.Sleep(500);
                file.GetThumbnail();
                file.GetIcon();
                DownloadedFiles.Add(file);
                FolderUpdate();
            }
        }

        private void _fw_OnRenamed(object sender, FileChangedEvent e)
        {
            var file = new DownloadedFile(e.FullPath);
            file.GetThumbnail();
            file.GetIcon();
            var oldFile = DownloadedFiles.FirstOrDefault(f => f.FilePath == e.OldFullPath);
            if (file != null && oldFile != null)
            {
                DownloadedFiles.Remove(oldFile);
                DownloadedFiles.Add(file);
                FolderUpdate();
            }
        }

        public List<DownloadedFile> DownloadedFiles { get; set; }
        public string FileContainFilter { get; set; }
        public void GetAllFiles()
        {
            var files = FileUtility.GetAllFilesInFolder(_folderPath).Where(p => Path.GetExtension(p) == _fileExtension
            && p.ToLower().Contains(FileContainFilter)).ToList();
            DownloadedFiles = new List<DownloadedFile>();
            files.ForEach(f => DownloadedFiles.Add(new DownloadedFile(f)));
        }
        private void FolderUpdate()
        {
            try
            {
                if (DrawingFinderPage.Main != null)
                    DrawingFinderPage.Main.Dispatcher.Invoke(delegate
                    {
                        var mainVM = (DrawingFinderVM)DrawingFinderPage.Main.DataContext;
                        if (mainVM != null)
                            mainVM.UpdateDownloadedFolder();
                    });

            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }

        }
        public void Dispose()
        {
            Fw.Dispose();
        }
    }
}
