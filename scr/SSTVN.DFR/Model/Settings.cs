﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Reflection;
using Serilog;
using Autodesk.Revit.DB;
using System.Windows.Media;

namespace SSTVN.DFR
{

    /// <summary>
    /// This Class stored the App settings
    /// </summary>
    public static class Settings
    {

        /// <summary>
        /// App working mode is Test mode or default mode: get;set;
        /// </summary>
        public static bool IsTestingMode
        {
            get
            {
                return Properties.Settings.Default.TestingMode;
            }
            set
            {
                Properties.Settings.Default.TestingMode = value;
                Properties.Settings.Default.Save();
            }
        }

        /// <summary>
        /// Is Hide Drawing finder before placing
        /// </summary>
        public static bool IsHideDrawingFinder
        {
            get
            {
                return Properties.Settings.Default.IsHideDrawingFinder;
            }
            set
            {
                Properties.Settings.Default.IsHideDrawingFinder = value;
                Properties.Settings.Default.Save();
            }
        }

        /// <summary>
        /// Is Keep Hide Drawing Finder After placing
        /// </summary>
        public static bool IsKeepHideDrawingFinderAfterPlacing
        {
            get
            {
                return Properties.Settings.Default.IsKeepHide;
            }
            set
            {
                Properties.Settings.Default.IsKeepHide = value;
                Properties.Settings.Default.Save();
            }
        }

        /// <summary>
        /// Get the current application version
        /// <remarks>
        ///     Will implement in the next version
        /// </remarks>
        /// </summary>
        //public static string CurrentVersion
        //{
        //    get
        //    {
        //        var version = Assembly.GetExecutingAssembly().GetName().Version;
        //        return string.Format("{0}.{1}.{2}", version.Major, version.Minor, version.Build);
        //    }
        //}


        /// <summary>
        /// Current default DWG downloaded folder
        /// </summary>
        public static string DefaultDWGDownloadedFolder { get; set; } = @"C:\Simpson Strong-Tie\Drawing Finder for AutoCAD\Drawings";

        /// <summary>
        /// Current default Revit downloaded folder
        /// </summary>
        public static string DefaultRFADownloadedFolder { get; set; } = @"C:\Simpson Strong-Tie\Drawing Finder for Revit\Drawings";

        /// <summary>
        /// DWG Downloaded Folder
        /// </summary>
        public static string DWGDownloadedFolder
        {
            get
            {
                string folder = Properties.Settings.Default.DwgFolder;

                if (string.IsNullOrEmpty(folder))
                {
                    folder = DefaultDWGDownloadedFolder;
                }

                Directory.CreateDirectory(folder);

                return folder;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Download location cannot be empty.");
                }

                if (value.Length > 115)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("Download location cannot longer than 115 chars. Now it's " + value.Length.ToString() + " chars.");
                    sb.Append("Please find another shorter location.");
                    throw new Exception(sb.ToString());
                }

                if (!Directory.Exists(value))
                {
                    if (value.Equals(DefaultDWGDownloadedFolder, StringComparison.InvariantCultureIgnoreCase))
                    {
                        Directory.CreateDirectory(value);
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.AppendLine("Download location doesn't exist: " + value);
                        sb.Append("Please create that folder first.");
                        throw new Exception(sb.ToString());
                    }
                }


                Properties.Settings.Default.DwgFolder = value;
                Properties.Settings.Default.Save();
            }
        }

        /// <summary>
        /// Revit Downloaded Folder
        /// </summary>
        public static string RFADownloadedFolder
        {
            get
            {
                string folder = Properties.Settings.Default.RevitFolder;

                if (string.IsNullOrEmpty(folder))
                {
                    folder = DefaultRFADownloadedFolder;
                }

                Directory.CreateDirectory(folder);

                return folder;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Download location cannot be empty.");
                }

                if (value.Length > 115)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("Download location cannot longer than 115 chars. Now it's " + value.Length.ToString() + " chars.");
                    sb.Append("Please find another shorter location.");
                    throw new Exception(sb.ToString());
                }

                if (!Directory.Exists(value))
                {
                    if (value.Equals(DefaultRFADownloadedFolder, StringComparison.InvariantCultureIgnoreCase))
                    {
                        Directory.CreateDirectory(value);
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.AppendLine("Download location doesn't exist: " + value);
                        sb.Append("Please create that folder first.");
                        throw new Exception(sb.ToString());
                    }
                }


                Properties.Settings.Default.RevitFolder = value;
                Properties.Settings.Default.Save();
            }
        }

        /// <summary>
        /// Testing link
        /// </summary>
        public static string TestingLink
        {
            get
            {
                return Properties.Settings.Default.TestingLink;
            }
            set
            {
                Properties.Settings.Default.TestingLink = value;
                Properties.Settings.Default.Save();
            }
        }

        /// <summary>
        /// Return revit link when default model, otherwise return test link
        /// </summary>
        public static string RevitLink => IsTestingMode ? TestingLink : Constants.RevitDrawingFinderWebPage;

        #region UI Color
        public static SolidColorBrush PanelBackGroundColorBrush { get; } = new SolidColorBrush(System.Windows.Media.Color.FromRgb(238, 238, 238));
        public static SolidColorBrush TabNameBackGroundColorBrush { get; } = new SolidColorBrush(System.Windows.Media.Color.FromRgb(204, 204, 204));
        public static SolidColorBrush TranparentColorBrush { get; } = new SolidColorBrush(System.Windows.Media.Colors.Transparent);
        #endregion

    }
}
