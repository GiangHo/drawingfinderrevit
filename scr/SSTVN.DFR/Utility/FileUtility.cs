﻿//
// (C) Copyright 2003-2017 by Autodesk, Inc.
//
// Permission to use, copy, modify, and distribute this software in
// object code form for any purpose and without fee is hereby granted,
// provided that the above copyright notice appears in all copies and
// that both that copyright notice and the limited warranty and
// restricted rights notice below appear in all supporting
// documentation.
//
// AUTODESK PROVIDES THIS PROGRAM "AS IS" AND WITH ALL FAULTS.
// AUTODESK SPECIFICALLY DISCLAIMS ANY IMPLIED WARRANTY OF
// MERCHANTABILITY OR FITNESS FOR A PARTICULAR USE. AUTODESK, INC.
// DOES NOT WARRANT THAT THE OPERATION OF THE PROGRAM WILL BE
// UNINTERRUPTED OR ERROR FREE.
//
// Use, duplication, or disclosure by the U.S. Government is subject to
// restrictions set forth in FAR 52.227-19 (Commercial Computer
// Software - Restricted Rights) and DFAR 252.227-7013(c)(1)(ii)
// (Rights in Technical Data and Computer Software), as applicable.
//


using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace SSTVN.DFR
{
    public class FileUtility
    {

        /// <summary>
        /// Check file is Expired
        /// </summary>
        /// <remarks>
        /// Return True if file is not exists or over 7 days since the last modified
        /// </remarks>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static bool IsFileExpired(string filePath)
        {
            // return true when file is not exits
            if (!File.Exists(filePath))
            {
                return true;
            }
            // return true if file expire 7 days
            DateTime theLastDayExpired = DateTime.Now.AddDays(-7);
            if (File.GetLastWriteTime(filePath) < theLastDayExpired)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        ///     Check File is exits
        /// </summary>
        /// <remarks>
        ///     return true when file is exists and file size larger than 1024B. Delete file has size smaller than 1024B
        /// </remarks>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static bool IsFileExists(string filePath)
        {
            if (File.Exists(filePath))
            {
                var size = new FileInfo(filePath).Length;
                if (size < 1024) // < 1 kB
                {
                    DeleteFile(filePath);
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Method get all file in a folder include sub folder.
        /// </summary>
        /// <param name="folderDirectory"></param>
        /// <returns></returns>
        public static List<string> GetAllFilesInFolder(string folderDirectory)
        {
            List<String> files = new List<String>();
            try
            {
                files.AddRange(System.IO.Directory.GetFiles(folderDirectory, "*.dwg", System.IO.SearchOption.AllDirectories));
                files.AddRange(System.IO.Directory.GetFiles(folderDirectory, "*.rfa", System.IO.SearchOption.AllDirectories));
            }
            catch (System.Exception ex)
            {
                Log.Error(ex.ToString());
            }
            return files;
        }

        /// <summary>
        /// Delete a file if it is not locked
        /// </summary>
        /// <param name="filePath"></param>
        public static void DeleteFile(string filePath)
        {

            if (!IsFileLocked(filePath))
            {
                File.Delete(filePath);
            }
            else
            {
                MessageBox.Show("File does not exist or is being used by another process", "Can not delete file", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Copy a file to the new path. Overwrite the old file if it is not locked
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="newFilePath"></param>
        public static void CopyFile(string filePath, string newFilePath)
        {
            try
            {
                // Create Directory
                CreateDirectory(Path.GetDirectoryName(newFilePath));
                // Copy file
                if (File.Exists(filePath) && IsFileLocked(newFilePath))
                {
                    System.IO.File.Copy(filePath, newFilePath, true);
                }
                else
                {
                    MessageBox.Show("New file is locked", "Can not copy file", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Can not copy file: " + filePath);
                throw ex;
            }
        }

        /// <summary>
        /// Open the dialog allow user browser for a folder
        /// </summary>
        /// <param name="previousFolder"></param>
        /// <returns></returns>
        public static string BrowserForAFolder(string previousFolder)
        {
            var frm = new FolderSelect.FolderSelectDialog();
            frm.Title = "Select folder to download drawings";
            if (System.IO.Directory.Exists(previousFolder))
                frm.InitialDirectory = previousFolder;
            if (frm.ShowDialog() == false)
                return null;
            return frm.FileName;
        }

        /// <summary>
        /// Create a directory
        /// </summary>
        /// <param name="directory"></param>
        public static void CreateDirectory(string directory)
        {
            try
            {
                // If the destination directory doesn't exist, create it.
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Can not Create Directory: " + directory);
                throw ex;
            }
        }

        /// <summary>
        /// Return true if file is locked
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static bool IsFileLocked(string filePath)
        {
            var file = new FileInfo(filePath);
            FileStream stream = null;
            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        /// <summary>
        /// Get assembly Path
        /// </summary>
        /// <returns></returns>
        public static String GetAssemblyPath()
        {
            if (string.IsNullOrEmpty(sm_assemblyPath))
                sm_assemblyPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            return sm_assemblyPath;
        }

        /// <summary>
        /// Get Assembly Name
        /// </summary>
        /// <returns></returns>
        public static String GetAssemblyFullName()
        {
            if (string.IsNullOrEmpty(sm_assemblyFullName))
                sm_assemblyFullName = Assembly.GetExecutingAssembly().Location;
            return sm_assemblyFullName;
        }

        /// <summary>
        /// Get Application resources Path
        /// </summary>
        /// <returns></returns>
        public static string GetApplicationResourcesPath()
        {
            if (string.IsNullOrEmpty(sm_appResourcePath))
                sm_appResourcePath = GetAssemblyPath() + "\\Resources\\";
            return sm_appResourcePath;
        }

        /// <summary>
        /// return downloaded file Path base on file Name
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string GetDownloadedFilePath(string fileName)
        {
            var fileType = Path.GetExtension(fileName);
            switch (fileType)
            {
                case ".dxf":
                case ".dwg":
                    return Path.Combine(Settings.DWGDownloadedFolder, fileName);
                case ".rfa":
                    return Path.Combine(Settings.RFADownloadedFolder, fileName);
                default:
                    return Path.Combine(Settings.RFADownloadedFolder, fileName);
            }
        }


        #region Data
        private static string sm_assemblyPath = null;
        private static string sm_assemblyFullName = null;
        private static string sm_appResourcePath = null;
        #endregion
    }
}
