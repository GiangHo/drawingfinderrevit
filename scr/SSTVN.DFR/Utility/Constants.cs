﻿//
// (C) Copyright 2003-2017 by Autodesk, Inc.
//
using Autodesk.Revit.UI;
using System;

namespace SSTVN.DFR
{
    public class Constants
    {
        #region Ribbon and Dockable Panel
        /// <summary>
        /// Application Name
        /// </summary>
        public const string ApplicationName = "Drawing Finder for Revit";

        /// <summary>
        /// Tab Name
        /// </summary>
        public const string DiagnosticsTabName = "Simpson Strong-Tie";

        /// <summary>
        /// Panel Group Name
        /// </summary>
        public const string DiagnosticsPanelName = "Simpson Strong-Tie Drawing Finder for Revit";

        /// <summary>
        /// Button Drawing Finder Name
        /// </summary>
        public const string BtnDrawingFinderName = "  Drawing  \nFinder";

        /// <summary>
        /// Button Drawing Finder ToolTip
        /// </summary>
        public const string BtnDrawingFinderToolTip = "Click here to be taken to the available Revit families from www.strongtie.com.";

        /// <summary>
        /// Button Drawing Finder Name
        /// </summary>
        public const string BtnDWGDrawingFinderName = "DWG\n  Import  ";

        /// <summary>
        /// Button Drawing Finder ToolTip
        /// </summary>
        public const string BtnDWGDrawingFinderToolTip = "Click here to be taken to the available DWG drawings from www.strongtie.com.";



        /// <summary>
        /// Button Downloaded Drawing Name
        /// </summary>
        public const string BtnDownloadedDrawingsName = "  Downloaded  \nDrawings";

        /// <summary>
        /// Button Downloaded Drawing Tooltip
        /// </summary>
        public const string BtnDownloadedDrawingsToolTip = "Click here to see all of the drawings you have downloaded from the application.";
        /// <summary>
        /// Button Other Applications Name
        /// </summary>
        public const string BtnOtherApplicationName = "Other\n  Applications  ";

        /// <summary>
        /// Button Other Applications ToolTip
        /// </summary>
        public const string BtnOtherApplicationToolTip = "Click here to see other applications provided by \nSimpson Strong-Tie.";

        /// <summary>
        /// Button Help Name
        /// </summary>
        public const string BtnHelpName = "Help";

        /// <summary>
        /// Button Help ToolTip
        /// </summary>
        public const string BtnHelpToolTip = "Help";

        /// <summary>
        /// Button Settings Name
        /// </summary>
        public const string BtnSettingsName = "Settings";

        /// <summary>
        /// Button Settings ToolTip
        /// </summary>
        public const string BtnSettingToolTip = "Simpson Strong-Tie Drawing Finder for Revit Settings.";


        /// <summary>
        /// Button Tutorial Name
        /// </summary>
        public const string BtnTutorialName = "Tutorial";

        /// <summary>
        /// Button Tutorial ToolTip
        /// </summary>
        public const string BtnTutorialToolTip = "Click here to be taken to a tutorial that will walk you through the best practices of using this tool.";

        /// <summary>
        /// Button About Name
        /// </summary>
        public const string BtnABoutName = "About";

        /// <summary>
        /// Button About ToolTip
        /// </summary>
        public const string BtnABoutToolTip = "About Simpson Strong-Tie Drawing Finder for Revit Plugin.";

        /// <summary>
        /// Button Request drawing / contact Name
        /// </summary>
        public const string BtnRequestName = "Drawing request / Contact us";

        /// <summary>
        /// Click here to send your request for additional drawings to us, or to report any issues you may have with the tool.
        /// </summary>
        public const string BtnRequestToolTip = "Drawing request / Contact us";

        /// <summary>
        /// Drawing Finder Dockpanel ID
        /// </summary>
        public static DockablePaneId DrawingFinderDockablePaneId = new DockablePaneId(new Guid("{7ad40b83-92c0-4722-953c-b0a44a00fae4}"));

        /// <summary>
        /// Downloaded drawings dockpanel ID
        /// </summary>
        public static DockablePaneId DownloadedDrawingsDockablePaneId = new DockablePaneId(new Guid("{239cc25f-789b-4d42-9515-d246429049f8}"));


        /// <summary>
        /// Downloaded drawings dockpanel ID
        /// </summary>
        public static string CefSharpFolder = @"C:\Program Files (x86)\Simpson Strong-Tie\Drawing Finder for Revit\Revit Res";
        #endregion

        #region Links

        /// <summary>
        /// Plugin introduction web pahe
        /// </summary>
        public const string PluginIntroductionWebPage = "https://www.strongtie.com/drawing/drawing-finder-for-revit";

        /// <summary>
        /// Link to Revit Drawing Finder Web Page
        /// </summary>
        public const string RevitDrawingFinderWebPage = "https://www.strongtie.com/resources/drawings?v=%3Arelevance%3AdrawingFileType%3ARevit&tab=drawing";

        /// <summary>
        /// Link to DWG Drawing Finder Web Page
        /// </summary>
        public const string DWGDrawingFinderWebPage = "https://www.strongtie.com/resources/drawings?v=%3Arelevance%3AdrawingFileType%3ADWG";

        /// <summary>
        /// Link to Other Application Web Page
        /// </summary>
        public const string OtherAppsWebPage = "http://www.strongtie.com/webapps";

        /// <summary>
        /// Link to Tutorial Web Page
        /// </summary>
        public const string TutorialWebPage = "https://p.widencdn.net/nvcfxj/Drawing-Finder-for-Revit---Best-Practices";

        /// <summary>
        /// Link to Request And Contact Web Page
        /// </summary>
        public const string RequestDrawingPage = "https://app.smartsheet.com/b/form/b1a76f4e52aa49af9dfa4364b91bb470";
        #endregion
    }
}
