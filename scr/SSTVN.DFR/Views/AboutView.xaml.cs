﻿using SSTVN.DFR.ViewModels;
using System.Windows;

namespace SSTVN.DFR.Views
{
    /// <summary>
    /// Interaction logic for About.xaml
    /// </summary>
    public partial class AboutView : Window
    {
        public AboutView()
        {
            InitializeComponent();
            this.DataContext = new AboutViewModel(this);
        }
    }
}
