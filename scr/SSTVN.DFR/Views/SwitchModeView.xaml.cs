﻿using SSTVN.DFR.ViewModels;
using System.Windows;

namespace SSTVN.DFR.Views
{
    /// <summary>
    /// Interaction logic for SwitchModeView.xaml
    /// </summary>
    public partial class SwitchModeView : Window
    {
        public SwitchModeView()
        {
            InitializeComponent();
            this.DataContext = new SwitchModeViewModel(this);
        }
    }
}
