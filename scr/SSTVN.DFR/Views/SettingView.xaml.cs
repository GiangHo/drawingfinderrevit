﻿using SSTVN.DFR.ViewModels;
using System.Windows;

namespace SSTVN.DFR.Views
{
    /// <summary>
    /// Interaction logic for SettingView.xaml
    /// </summary>
    public partial class SettingView : Window
    {
        public SettingView()
        {
            InitializeComponent();
            this.DataContext = new SettingsViewModel(this);
        }

    }
}
