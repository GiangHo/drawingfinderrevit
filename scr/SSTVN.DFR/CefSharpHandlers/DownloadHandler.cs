﻿using CefSharp;
using SSTVN.DFR.ViewModels;
using System;

namespace SSTVN.DFR
{

    /// <summary>
    /// The class Inherit IDownloadHandler
    /// </summary>
    public class DownloadHandler : IDownloadHandler
    {

        public event EventHandler<DownloadItem> OnBeforeDownloadFired;

        public event EventHandler<DownloadItem> OnDownloadUpdatedFired;


        public DownloadHandler()
        {
        }

        public void OnBeforeDownload(IBrowser browser, DownloadItem downloadItem, IBeforeDownloadCallback callback)
        {
            try
            {
                var filePath = string.Empty;

                filePath = FileUtility.GetDownloadedFilePath(downloadItem.SuggestedFileName);
                if (!FileUtility.IsFileExpired(filePath))
                    callback.Dispose();

                if (!callback.IsDisposed)
                    OnBeforeDownloadFired?.Invoke(this, downloadItem);
                else
                {

                    DrawingFinderPage.Main.Dispatcher.Invoke(delegate
                    {
                        var mainVM = (DrawingFinderVM)DrawingFinderPage.Main.DataContext;
                        //DrawingFinderPage._main.ListViewDrawings.ItemsSource = DownloadedFiles;
                        if (mainVM != null)
                            mainVM.LoadFileToRevit(filePath);
                    });
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.ToString());
            }
        }

        public void OnDownloadUpdated(IBrowser browser, DownloadItem downloadItem, IDownloadItemCallback callback)
        {
            OnDownloadUpdatedFired?.Invoke(this, downloadItem);
        }


    }
}
